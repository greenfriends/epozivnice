<?php

namespace Epozivnice\Backend\Action;

use Epozivnice\Template\Repository\TemplateRepository as TemplateRepo;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface as Logger;
use Laminas\Config\Config;
use \League\Plates\Engine;
use Skeletor\Action\Web\Html;
use Laminas\Session\ManagerInterface as Session;
use Tamtamchik\SimpleFlash\Flash;

class Template extends Html
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var Flash
     */
    private $flash;

    private $repo;

    /**
     * HomeAction constructor.
     * @param Logger $logger
     * @param Config $config
     * @param Engine $template
     */
    public function __construct(
        Logger $logger, Config $config, Engine $template, Session $session, Flash $flash, TemplateRepo $repo
    ) {
        parent::__construct($logger, $config, $template);
        $this->flash = $flash;
        $this->session = $session;
        $this->repo = $repo;
    }

    /**
     * Parses data for provided merchantId
     *
     * @param ServerRequestInterface $request request
     * @param ResponseInterface $response response
     *
     * @return ResponseInterface
     * @throws \Exception
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response) {
        return $this->respond('template/single', [
            'template' => $this->repo->getBySlug($request->getAttribute('slug')),
            'templates' => $this->repo->fetchAll([], 8)
        ]);
    }

}