<?php

namespace Epozivnice\Backend\Action;

use Epozivnice\Template\Repository\TemplateRepository as TemplateRepo;
use Psr\Log\LoggerInterface as Logger;
use Laminas\Config\Config;
use \League\Plates\Engine;
use Skeletor\Action\Web\Html;
use Laminas\Session\ManagerInterface as Session;
use Tamtamchik\SimpleFlash\Flash;

class Checkout extends Html
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var Flash
     */
    private $flash;

    private $repo;

    /**
     * HomeAction constructor.
     * @param Logger $logger
     * @param Config $config
     * @param Engine $template
     */
    public function __construct(
        Logger $logger, Config $config, Engine $template, Session $session, Flash $flash, TemplateRepo $repo
    ) {
        parent::__construct($logger, $config, $template);
        $this->flash = $flash;
        $this->session = $session;
        $this->repo = $repo;
    }

    /**
     * Parses data for provided merchantId
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request request
     * @param \Psr\Http\Message\ResponseInterface $response response
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function __invoke(
        \Psr\Http\Message\ServerRequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response
    ) {
        $template = $this->repo->getById((int) $request->getQueryParams()['templateId']);
        $color = $request->getQueryParams()['color'];
        $errors = $this->session->getStorage()->offsetGet('errors');
        $this->session->getStorage()->offsetUnset('errors');
        $formData = $this->session->getStorage()->offsetGet('formData');
        $this->session->getStorage()->offsetUnset('formData');
        $tpl = 'template/checkoutWedding';
        if ($template->getType() !== \Epozivnice\Template\Model\Template::TYPE_WEDDING) {
            $tpl = 'template/checkoutBirthday';
        }
//        $this->setGlobalVariable('cssFile', '/'. $tplData['templateId'] .'/'. $template->getCssFile() . '.css');

        return $this->respond($tpl, [
            'template' => $template,
            'color' => $color,
            'errors' => $errors,
            'formData' => $formData
        ]);
    }
}