<?php

namespace Epozivnice\Backend\Action;

use Epozivnice\Template\Repository\TemplateRepository as TemplateRepo;
use Epozivnice\Event\Repository\EventRepository as EventRepo;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface as Logger;
use Laminas\Config\Config;
use \League\Plates\Engine;
use Skeletor\Action\Web\Html;
use Laminas\Session\ManagerInterface as Session;
use Tamtamchik\SimpleFlash\Flash;
use Epozivnice\Event\Filter\Event;
use Skeletor\Image\Service\Image;

class Payment extends Html
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var Flash
     */
    private $flash;

    private $repo;

    private $filter;

    private $image;

    private $eventRepo;

    /**
     * HomeAction constructor.
     * @param Logger $logger
     * @param Config $config
     * @param Engine $template
     */
    public function __construct(
        Logger $logger, Config $config, Engine $template, Session $session, Flash $flash, TemplateRepo $repo,
        EventRepo $eventRepo, Event $filter, Image $image
    ) {
        parent::__construct($logger, $config, $template);
        $this->flash = $flash;
        $this->session = $session;
        $this->repo = $repo;
        $this->image = $image;
        $this->filter = $filter;
        $this->eventRepo = $eventRepo;
    }

    /**
     * Parses data for provided merchantId
     *
     * @param ServerRequestInterface $request request
     * @param ResponseInterface $response response
     *
     * @return ResponseInterface
     * @throws \Exception
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response)
    {
        $data = $request->getQueryParams();

        return $this->respond('template/payment', [
            'template' => $this->repo->getById($data['templateId']),
            'event' => $this->eventRepo->getById($data['eventId'])
        ]);
    }

}