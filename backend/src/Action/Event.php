<?php

namespace Epozivnice\Backend\Action;

use Epozivnice\Event\Repository\EventRepository;
use Epozivnice\Template\Repository\TemplateRepository;
use Psr\Log\LoggerInterface as Logger;
use Laminas\Config\Config;
use \League\Plates\Engine;
use Skeletor\Action\Web\Html;
use Laminas\Session\ManagerInterface as Session;
use Skeletor\Mapper\NotFoundException;
use Tamtamchik\SimpleFlash\Flash;

class Event extends Html
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var Flash
     */
    private $flash;

    private $repo;

    private $tplRepo;

    /**
     * HomeAction constructor.
     * @param Logger $logger
     * @param Config $config
     * @param Engine $template
     */
    public function __construct(
        Logger $logger, Config $config, Engine $template, Session $session, Flash $flash, EventRepository $repo,
        TemplateRepository $tplRepo
    ) {
        parent::__construct($logger, $config, $template);
        $this->flash = $flash;
        $this->session = $session;
        $this->repo = $repo;
        $this->tplRepo = $tplRepo;
    }

    /**
     * Print single event page
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request request
     * @param \Psr\Http\Message\ResponseInterface $response response
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function __invoke(
        \Psr\Http\Message\ServerRequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response
    ) {
        try {
            $key = $request->getAttribute('key');
            $event = $this->repo->getBySlug($request->getAttribute('slug'), $key);
            $this->setGlobalVariable('key', $key);
        } catch (NotFoundException $e) {
            $this->flash->error($e->getMessage());
            return $response
                ->withStatus(302)
                ->withHeader('Location', $this->getConfig()->offsetGet('baseUrl'));
        } catch (\Exception $e) {

        }

        $this->setGlobalVariable('title', $event->getHost1FirstName() .' i '. $event->getHost2FirstName());
        $initials = $event->getHost1FirstName()[0] .'+'. $event->getHost2FirstName()[0];
        $tplData = $this->repo->getByEvent($event->getId());
        $template = $this->tplRepo->getById($tplData['templateId']);
        $this->setGlobalVariable('cssFile', '/assets/css/templates/'. $template->getCssFile() . '.css');
        if ($template->getJsFile() !== '') {
            $this->setGlobalVariable('jsFile', '/assets/css/templates/'. $template->getJsFile() . '.js');
        }
        $this->setGlobalVariable('bodyColor', $event->getTemplate()->getColor());

        return $this->respond('event/' . $template->getTemplateFile(), [
            'event' => $event, 'initials' => $initials,
        ]);
    }

}