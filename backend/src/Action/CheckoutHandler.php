<?php

namespace Epozivnice\Backend\Action;

use Epozivnice\Template\Repository\TemplateRepository as TemplateRepo;
use Epozivnice\Event\Repository\EventRepository as EventRepo;
use GuzzleHttp\Psr7\UploadedFile;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface as Logger;
use Laminas\Config\Config;
use \League\Plates\Engine;
use Skeletor\Action\Web\Html;
use Laminas\Session\ManagerInterface as Session;
use Skeletor\Validator\ValidatorException;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Image\Service\Image;

class CheckoutHandler extends Html
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var Flash
     */
    private $flash;

    private $repo;

    private $filter;

    private $image;

    private $eventRepo;

    /**
     * HomeAction constructor.
     * @param Logger $logger
     * @param Config $config
     * @param Engine $template
     */
    public function __construct(
        Logger $logger, Config $config, Engine $template, Session $session, Flash $flash, TemplateRepo $repo,
        EventRepo $eventRepo, \Epozivnice\Event\Filter\Event $filter, Image $image
    ) {
        parent::__construct($logger, $config, $template);
        $this->flash = $flash;
        $this->session = $session;
        $this->repo = $repo;
        $this->image = $image;
        $this->filter = $filter;
        $this->eventRepo = $eventRepo;
    }

    /**
     * Parses data for provided merchantId
     *
     * @param ServerRequestInterface $request request
     * @param ResponseInterface $response response
     *
     * @return ResponseInterface
     * @throws \Exception
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response)
    {
        try {
            $data = $this->filter->filter($request);
            $files = $request->getUploadedFiles();
            $entity = $this->eventRepo->create($data);
            $this->image = $this->image->setEntityPath('event');
            /** @var UploadedFile $file */
            foreach($files as $key => $file) {
                if ($file->getClientFilename() !== '') {
                    $data[$key] = $this->image->processUploadedFile($file, $entity->getId());
                }
            }
            $data['eventId'] = $entity->getId();
            $data['slug'] = $entity->getSlug();
            $this->eventRepo->update($data);
        } catch (ValidatorException $e) {
            $this->session->getStorage()->offsetSet('errors', $this->filter->getErrors());
            $this->session->getStorage()->offsetSet('formData', unserialize($e->getMessage()));
            $data = $request->getParsedBody();
            $url = sprintf('/checkout/?templateId=%s&color=%s', $data['templateId'], $data['color']);
            return $response
                ->withStatus(302)
                ->withHeader('Location', $this->getConfig()->offsetGet('baseUrl') . $url);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
        return $response
            ->withStatus(302)
            ->withHeader('Location', $this->getConfig()->offsetGet('baseUrl') .
                sprintf('/payment/?templateId=%s&eventId=%s', $entity->getTemplate()->getTemplateId(), $entity->getId()));
    }

}