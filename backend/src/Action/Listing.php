<?php

namespace Epozivnice\Backend\Action;

use Epozivnice\Event\Repository\EventRepository;
use Epozivnice\Template\Repository\TemplateRepository;
use Psr\Log\LoggerInterface as Logger;
use Laminas\Config\Config;
use \League\Plates\Engine;
use Skeletor\Action\Web\Html;
use Laminas\Session\ManagerInterface as Session;
use Tamtamchik\SimpleFlash\Flash;

class Listing extends Html
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var Flash
     */
    private $flash;

    private $repo;

    private $tplRepo;

    /**
     * HomeAction constructor.
     * @param Logger $logger
     * @param Config $config
     * @param Engine $template
     */
    public function __construct(
        Logger $logger, Config $config, Engine $template, Session $session, Flash $flash, EventRepository $repo,
        TemplateRepository $tplRepo
    ) {
        parent::__construct($logger, $config, $template);
        $this->flash = $flash;
        $this->session = $session;
        $this->repo = $repo;
        $this->tplRepo = $tplRepo;
    }

    /**
     * Parses data for provided merchantId
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request request
     * @param \Psr\Http\Message\ResponseInterface $response response
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function __invoke(
        \Psr\Http\Message\ServerRequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response
    ) {
        $page = $request->getAttribute('page');
        $params = $request->getQueryParams();
        $perPage = 10;
        $this->setGlobalVariable('title', 'some title');
        $filter = [];
        $limit = null;
        if ($page) {
            $limit = [
                'offset' => ($page-1) * $perPage,
                'limit' => $perPage
            ];
        }
        if (isset($params['category'])) {
            $filter['type'] = (int) $params['category'];
        }

        return $this->respond('template/listing', [
            'templates' => $this->tplRepo->fetchAll($filter, $limit),
            'counts' => $this->tplRepo->getStats(),
            'filter' => $filter
        ]);
    }

}