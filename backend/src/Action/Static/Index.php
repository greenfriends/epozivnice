<?php
declare(strict_types=1);
namespace Epozivnice\Backend\Action\Static;

use Laminas\Config\Config;
use League\Plates\Engine;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Action\Web\Html;
use Epozivnice\Template\Repository\TemplateRepository;

class Index extends Html
{
    private $repo;

    public function __construct(Logger $logger, Config $config, Engine $template, TemplateRepository $repo)
    {
        parent::__construct($logger, $config, $template);
        $this->repo = $repo;
    }

    /**
     * Parses data for provided merchantId
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request request
     * @param \Psr\Http\Message\ResponseInterface $response response
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response)
    {
        return $this->respond('static/home', ['templates' => $this->repo->fetchAll([], 8)]);
    }
}