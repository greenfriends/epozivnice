<?php
declare(strict_types=1);
namespace Epozivnice\Backend\Action\Static;

use Skeletor\Action\Web\Html;

class About extends Html
{
    /**
     * Display about us page
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request request
     * @param \Psr\Http\Message\ResponseInterface $response response
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function __invoke(
        \Psr\Http\Message\ServerRequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response
    ) {
        return $this->respond('static/about', []);
    }
}
