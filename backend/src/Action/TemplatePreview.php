<?php

namespace Epozivnice\Backend\Action;

use Epozivnice\Template\Repository\TemplateRepository as TemplateRepo;
use Psr\Log\LoggerInterface as Logger;
use Laminas\Config\Config;
use \League\Plates\Engine;
use Skeletor\Action\Web\Html;
use Laminas\Session\ManagerInterface as Session;
use Tamtamchik\SimpleFlash\Flash;

class TemplatePreview extends Html
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var Flash
     */
    private $flash;

    private $repo;

    /**
     * HomeAction constructor.
     * @param Logger $logger
     * @param Config $config
     * @param Engine $template
     */
    public function __construct(
        Logger $logger, Config $config, Engine $template, Session $session, Flash $flash, TemplateRepo $repo
    ) {
        parent::__construct($logger, $config, $template);
        $this->flash = $flash;
        $this->session = $session;
        $this->repo = $repo;
    }

    /**
     * Parses data for provided merchantId
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request request
     * @param \Psr\Http\Message\ResponseInterface $response response
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function __invoke(
        \Psr\Http\Message\ServerRequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response
    ) {

        $template = $this->repo->getById((int) $request->getAttribute('id'));
        $this->setGlobalVariable('title', $template->getLabel());
        $this->setGlobalVariable('cssFile', '/assets/css/templates/'. $template->getCssFile() . '.css');
        if ($template->getJsFile() !== '') {
            $this->setGlobalVariable('jsFile', '/assets/css/templates/'. $template->getJsFile() . '.js');
        }
        $this->setGlobalVariable('bodyColor', '');

        return $this->respond('event/' . $template->getTemplateFile() . 'Preview', [
//            'template' => $this->repo->getBySlug($request->getAttribute('slug'))
        ]);
    }

}