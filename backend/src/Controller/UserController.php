<?php
declare(strict_types=1);

namespace Epozivnice\Backend\Controller;

use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;
use Skeletor\User\Service\User as UserService;
use Skeletor\User\Filter\User;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Validator\ValidatorException;
use League\Plates\Engine;

/**
 * Class UserController
 * @package Epozivnice\Backend\Controller
 */
class UserController extends \Skeletor\User\Controller\UserController
{
    const TITLE_VIEW = "View users";
    const TITLE_CREATE = "Create user";
    const TITLE_UPDATE = "Edit user: ";
    const TITLE_UPDATE_SUCCESS = "User updated successfully.";
    const TITLE_CREATE_SUCCESS = "User created successfully.";
    const TITLE_DELETE_SUCCESS = "User deleted successfully.";

    private $logger;

    public function __construct(
        UserService $userService, Session $session, Config $config, Flash $flash, Engine $template, User $userFilter,
        LoggerInterface $logger)
    {
        parent::__construct($userService, $session, $config, $flash, $template, $userFilter);

        $this->logger = $logger;
    }

    /**
     * User list & view
     *
     * @return Response
     */
    public function view(): Response
    {
        $filter = [];
        $this->setGlobalVariable('pageTitle', 'View Users');
        if ($this->getSession()->getStorage()->offsetGet('loggedInRole') !== \Skeletor\User\Model\User::ROLE_ADMIN) {
            $filter = ['tenantId' => $this->getSession()->getStorage()->offsetGet('tenantId')];
        }
        $data = $this->getRequest()->getQueryParams();
        if (isset($data['tenantFilter'])) {
            $filter['tenantId'] = $data['tenantFilter'];
        }
        if (isset($data['status'])) {
            $filter['isActive'] = $data['status'];
        }
        if (isset($data['type'])) {
            $filter['role'] = $data['type'];
        }
        $users = $this->userService->getUsers($filter);

        return $this->respond('view', [
            'users' => $users,
            'filters' => $filter
        ]);
    }

    public function form(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('userId');
        if ($this->getSession()->getStorage()->offsetGet('loggedInRole') !== \Skeletor\User\Model\User::ROLE_ADMIN) {
            if ($id !== $this->getSession()->getStorage()->offsetGet('loggedIn')) {
                $this->getFlash()->error('Can only edit owned profile.');

                return $this->redirect(sprintf('/user/form/%s/', $this->getSession()->getStorage()->offsetGet('loggedIn')));
            }
        }
        $user = null;
        $this->setGlobalVariable('pageTitle', self::TITLE_CREATE);
        if ($id) {
            $user = $this->userService->getById($id);
            $this->setGlobalVariable('pageTitle', self::TITLE_UPDATE . $user->getDisplayName());
        }

        return $this->respond('form', [
            'user' => $user,
        ]);
    }

    /**
     * Added address to user.
     *
     * @return Response
     */
    public function update(): Response
    {
        try {
            $data = $this->userFilter->filter($this->getRequest());
            $user = $this->userService->update($data);
        } catch (ValidatorException $e) {
            $this->parseErrors();
            return $this->redirect(sprintf('/user/form/%s/', $this->getRequest()->getAttribute('userId')));
        } catch (\Exception $e) {
            $this->getFlash()->error($e->getMessage());
            return $this->redirect(sprintf('/user/form/%s/', $this->getRequest()->getAttribute('userId')));
        }
        $this->getFlash()->success(self::TITLE_UPDATE_SUCCESS);
        return $this->redirect(sprintf('/user/view/%s/', $user->getId()));
    }

    public function create(): Response
    {
        try {
            $data = $this->userFilter->filter($this->getRequest());
            $user = $this->userService->create($data);
        } catch (ValidatorException $e) {
            $this->parseErrors();
            return $this->redirect('/user/form/');
        } catch (\Exception $e) {
            $this->getFlash()->error($e->getMessage());
            $this->logger->debug($e->getTraceAsString());
            return $this->redirect('/user/form/');
        }
        $this->getFlash()->success(self::TITLE_CREATE_SUCCESS);
        return $this->redirect('/user/view/');
    }

    public function delete(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('userId');
        $this->userService->delete($id);
        $this->getFlash()->success(self::TITLE_DELETE_SUCCESS);

        return $this->redirect('/user/view/');
    }

    private function parseErrors()
    {
        foreach ($this->userFilter->getErrors() as $key => $messages) {
            foreach ($messages as $message) {
                $this->getFlash()->error($message);
            }
        }
    }
}
