<?php
declare(strict_types=1);
namespace Epozivnice\Backend\Controller;

use Skeletor\Translator\Service\Translator;
use Skeletor\User\Service\User;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Controller\CrudController;
use Psr\Log\LoggerInterface as Logger;
use League\Plates\Engine;
use Skeletor\Activity\Service\Activity;

class TranslationController extends CrudController
{
    const TITLE_VIEW = "View translations";
    const TITLE_CREATE = "Create new translation";
    const TITLE_UPDATE = "Edit translation: ";
    const TITLE_UPDATE_SUCCESS = "Translation updated successfully.";
    const TITLE_CREATE_SUCCESS = "Translation created successfully.";
    const TITLE_DELETE_SUCCESS = "Translation deleted successfully.";
    const PATH = 'translator';

    private $translator;

    /**
     * @param Translator $translator
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param Engine $template
     * @param User $userService
     * @param Logger $logger
     */
    public function __construct(
        Translator $translator, Session $session, Config $config, Flash $flash,
        Engine $template, User $userService, Logger $logger, Activity $activity
    ) {
        parent::__construct($translator->getRepository(), $session, $config, $flash, $template, $logger, null, $activity);

        $this->translator = $translator;
    }

}