<?php
declare(strict_types=1);
namespace Epozivnice\Backend\Controller;

use Epozivnice\Template\Model\Template;
use Epozivnice\Template\Repository\TemplateRepository;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\UploadedFile;
use Skeletor\Image\Service\Image;
use Laminas\Config\Config;
use Laminas\Session\SessionManager as Session;
use League\Plates\Engine;
use Psr\Log\LoggerInterface as Logger;
use Skeletor\Controller\CrudController;
use Skeletor\Validator\ValidatorException;
use Tamtamchik\SimpleFlash\Flash;

class TemplateController extends CrudController
{
    const TITLE_VIEW = "View templates";
    const TITLE_CREATE = "Create new template";
    const TITLE_UPDATE = "Edit template: ";
    const TITLE_UPDATE_SUCCESS = "Template updated successfully.";
    const TITLE_CREATE_SUCCESS = "Template created successfully.";
    const TITLE_DELETE_SUCCESS = "Template deleted successfully.";
    const PATH = 'template';

    /**
     * @param TemplateRepository $templateRepo
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param Engine $template
     * @param Logger $logger
     */
    public function __construct(
        TemplateRepository $templateRepo, Session $session, Config $config, Flash $flash, Engine $template,
        Logger $logger, Image $image
    ) {
        parent::__construct($templateRepo, $session, $config, $flash, $template, $logger);
        $this->image = $image->setEntityPath(static::PATH);
    }

    public function form(): Response
    {
        $id = (int) $this->getRequest()->getAttribute('id');
        $model = null;
        $errors = null;
        $this->setGlobalVariable('pageTitle', static::TITLE_CREATE);
        if ($id) {
            /* @var Template $model */
            $model = $this->getRepository()->getById($id);
            $title = $model->getLabel();
            $this->setGlobalVariable('pageTitle', static::TITLE_UPDATE . $title);
            $errors = $this->checkForFiles($model);
        }

        return $this->respond('form', [
            'model' => $model,
            'errors' => $errors,
        ]);
    }

    /**
     * @param Template $template
     * @return string
     */
    private function checkForFiles(Template $template)
    {
        if (strlen($template->getJsFile()) > 0 &&
            !is_file(PUBLIC_PATH . '/assets/css/templates/' . $template->getJsFile() . '.js')) {
            $this->getFlash()->error('Js file cannot be loaded.');
        }
        if (!is_file(PUBLIC_PATH . '/assets/css/templates/' . $template->getCssFile() . '.css')) {
            $this->getFlash()->error('Css file cannot be loaded.');
        }
        if (!is_file(APP_PATH . '/../themes/admin/event/' . $template->getTemplateFile() . '.php')) {
            $this->getFlash()->error('Template file cannot be loaded.');
        }
        return $this->getFlash()->display();
    }

    /**
     * @return Response
     */
    public function update(): Response
    {
        try {
//            $data = $this->filter->filter($this->getRequest());
            $data = $this->getRequest()->getParsedBody();
            $data['filters'] = serialize(['colors' => $data['colors']]);
            unset($data['colors']);
            $files = $this->getRequest()->getUploadedFiles();
            /** @var UploadedFile $file */
            foreach($files as $key => $file) {
                if ($file->getClientFilename() !== '') {
                    $data[$key] = $this->image->processUploadedFile($file, $data['templateId']);
                }
            }
            $this->getRepository()->update($data);
            $this->getFlash()->success(static::TITLE_UPDATE_SUCCESS);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
        return $this->redirect(sprintf('/admin/%s/view/', static::PATH));
    }

    public function create(): Response
    {
        try {
//            $data = $this->filter->filter($this->getRequest());
            $data = $this->getRequest()->getParsedBody();
            $files = $this->getRequest()->getUploadedFiles();
            $data['filters'] = serialize(['colors' => $data['colors']]);
            unset($data['colors']);
            $entity = $this->getRepository()->create($data);
            /** @var UploadedFile $file */
            foreach($files as $key => $file) {
                if ($file->getClientFilename() !== '') {
                    $data[$key] = $this->image->processUploadedFile($file, $entity->getId());
                }
            }
            $data['templateId'] = $entity->getId();
            $data['slug'] = $entity->getSlug();
            $this->getRepository()->update($data);
            $this->getFlash()->success(static::TITLE_CREATE_SUCCESS);
        } catch (ValidatorException $e) {

            return $this->redirect(sprintf('/admin/%s/form/', static::PATH));
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
        return $this->redirect(sprintf('/admin/%s/view/', static::PATH));
    }
}