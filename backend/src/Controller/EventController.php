<?php
declare(strict_types=1);
namespace Epozivnice\Backend\Controller;

use Epozivnice\Event\Filter\Event;
use Epozivnice\Event\Service\Mailer;
use Skeletor\Image\Service\Image;
use GuzzleHttp\Psr7\UploadedFile;
use Skeletor\User\Service\User;
use GuzzleHttp\Psr7\Response;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use Skeletor\Validator\ValidatorException;
use Tamtamchik\SimpleFlash\Flash;
use Skeletor\Controller\CrudController;
use Psr\Log\LoggerInterface as Logger;
use League\Plates\Engine;
use Epozivnice\Event\Repository\EventRepository;

class EventController extends CrudController
{
    const TITLE_VIEW = "View event";
    const TITLE_CREATE = "Create new event";
    const TITLE_UPDATE = "Edit event: ";
    const TITLE_UPDATE_SUCCESS = "Event updated successfully.";
    const TITLE_CREATE_SUCCESS = "Event created successfully.";
    const TITLE_DELETE_SUCCESS = "Event deleted successfully.";
    const PATH = 'event';

    /**
     * @var User
     */
    private $userService;

    private $mailer;

    /**
     * @param EventRepository $repo
     * @param Session $session
     * @param Config $config
     * @param Flash $flash
     * @param Engine $template
     * @param User $userService
     * @param Logger $logger
     */
    public function __construct(
        EventRepository $repo, Session $session, Config $config, Flash $flash, Engine $template,
        User $userService, Logger $logger, Event $filter, Image $image, Mailer $mailer
    ) {
        parent::__construct($repo, $session, $config, $flash, $template, $logger, $filter);
        $this->image = $image->setEntityPath(static::PATH);
        $this->mailer = $mailer;
//        $this->userService = $userService;
    }

    /**
     * @return Response
     */
    public function view(): Response
    {
        $this->setGlobalVariable('pageTitle', self::TITLE_VIEW);
        $filter = [];
        if ($this->getSession()->getStorage()->offsetGet('loggedInRole') !== \Skeletor\User\Model\User::ROLE_ADMIN) {
            $filter = ['tenantId' => $this->getSession()->getStorage()->offsetGet('tenantId')];
        }
        $data = $this->getRequest()->getQueryParams();
        if (isset($data['tenantFilter'])) {
            $filter['tenantId'] = $data['tenantFilter'];
        }
        if (isset($data['userId'])) {
            $filter['userId'] = $data['userId'];
        }

        return $this->respond('view', [
            'models' => $this->getRepository()->fetchAll($filter),
            'filters' => $filter,
        ]);
    }

    public function update(): Response
    {
        try {
            $data = $this->filter->filter($this->getRequest());
            $files = $this->getRequest()->getUploadedFiles();
            /** @var UploadedFile $file */
            foreach($files as $key => $file) {
                if ($file->getClientFilename() !== '') {
                    $data[$key] = $this->image->processUploadedFile($file, $data['eventId']);
                }
            }
            $this->getRepository()->update($data);
            $this->getFlash()->success(static::TITLE_UPDATE_SUCCESS);
        } catch (\Exception $e) {
            var_dump($this->filter->getErrors());
            var_dump($e->getMessage());
            die();
        }
        return $this->redirect(sprintf('/admin/%s/view/', static::PATH));
    }

    public function create(): Response
    {
        try {
            $data = $this->filter->filter($this->getRequest());
            $files = $this->getRequest()->getUploadedFiles();
            $entity = $this->getRepository()->create($data);
            /** @var UploadedFile $file */
            foreach($files as $key => $file) {
                if ($file->getClientFilename() !== '') {
                    $data[$key] = $this->image->processUploadedFile($file, $entity->getId());
                }
            }
            $data['eventId'] = $entity->getId();
            $data['slug'] = $entity->getSlug();
            $this->getRepository()->update($data);
            $this->getFlash()->success(static::TITLE_CREATE_SUCCESS);
        } catch (ValidatorException $e) {

            return $this->redirect(sprintf('/admin/%s/form/', static::PATH));
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
        }
        return $this->redirect(sprintf('/admin/%s/view/', static::PATH));
    }

    public function status()
    {
        $status = (int) $this->getRequest()->getQueryParams()['status'];
        $event = $this->getRepository()->getById($this->getRequest()->getAttribute('id'));
        $this->getRepository()->setStatus($status, $this->getRequest()->getAttribute('id'));
        $this->getFlash()->success('Status uspešno izmenjen.');
        if ($status === \Epozivnice\Event\Model\Event::STATUS_PAID) {
            $this->mailer->sendEventPrivateLink(
                $event->getUserName(),
                $this->getConfig()->offsetGet('baseUrl') .'/'. $event->getUri(),
                $event->getEmail(),
                $event->getHash());
        }

        return $this->redirect(sprintf('/admin/%s/view/', static::PATH));
    }
}