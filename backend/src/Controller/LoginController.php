<?php
declare(strict_types=1);
namespace Epozivnice\Backend\Controller;

use Skeletor\User\Service\User as UserService;
use Skeletor\User\Filter\Login;
use Laminas\Session\SessionManager as Session;
use Laminas\Config\Config;
use Tamtamchik\SimpleFlash\Flash;
use League\Plates\Engine;

class LoginController extends \Skeletor\User\Controller\LoginController
{
    const LOGGED_OUT = 'Uspešno ste se izlogovali.';

    public function __construct(
        UserService $userService, Session $session, Config $config, Flash $flash, Engine $template, Login $loginFilter
    ) {
        parent::__construct($userService, $session, $config, $flash, $template, $loginFilter);
    }
}