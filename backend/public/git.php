<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

include("constants.php");

exec('git log -n 1', $output);
$targetFile = DATA_PATH . '/.gitLog';
if (!is_file($targetFile)) {
    touch($targetFile);
}

file_put_contents($targetFile, substr(str_replace('commit ', '', $output[0]), 0, 6));