import Invoice from "./Invoice.js";
import Client from "./Client.js";
import Service from "./Service.js";

$(document).ready(function() {
    let userId = $('#userId').attr('data-userId');

    // extend original append event
    (function($) {
        var origAppend = $.fn.append;

        $.fn.append = function () {
            return origAppend.apply(this, arguments).trigger("append");
        };
    })(jQuery);

    //moment.js initialize
    // moment().format();

    $('#usersTable').DataTable({
        "order": [[ 5, "desc" ]],
        "pageLength": 25
    });
    $('#tenantTable').DataTable({
        "order": [[ 2, "desc" ]],
        "pageLength": 25
    });
    $('#clientTable').DataTable({
        "order": [[ 2, "desc" ]],
        "pageLength": 25
    });
    $('#activityTable').DataTable({
        "order": [[ 5, "desc" ]],
        "pageLength": 25
    });
    $('#servicesTable').DataTable({
        "order": [[ 5, "desc" ]],
        "pageLength": 25
    });
    $('#translatorTable').DataTable({
        "order": [[ 5, "desc" ]],
        "pageLength": 25
    });

    $('.filter').click(function() {
        filter();
    });

    $('.datepicker').datepicker({
        dateFormat: 'yy-m-d'
    });

    //check all
    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });

    if (document.getElementById('serviceForm')) {
        let servicePage = new Service();
    }

    if (document.getElementById('clientForm')) {
        let clientPage = new Client();
        clientPage.switchClientType();
        document.getElementsByClassName('clientType')[0].addEventListener('change', function() {
            clientPage.switchClientType();
        });
    }

    if (document.getElementById('invoiceForm')) {
        let invoicePage = new Invoice();
        invoicePage.fillClientInfoForInvoice();
        invoicePage.loadSelectedServices();
    }

});


function filter() {
    // var urlParams = new URLSearchParams(window.location.search);
    var urlParams = new URLSearchParams();
    var statusFilter = [];

    // $('.statusPicker').each(function(i, v) {
    //     if ($(v).prop('checked')) {
    //         statusFilter.push($(v).val());
    //     }
    // });
    if ($('.statusFilter').val()) {
        statusFilter.push($('.statusFilter').val());
    }

    statusFilter = statusFilter.join(',');
    if ($('.roleFilter').val() !== '' && $('.roleFilter').val() !== undefined) {
        urlParams.set('type', $('.roleFilter').val());
    }
    if ($('.dateFrom').val() !== '' && $('.dateFrom').val() !== undefined) {
        urlParams.set('dateFrom', $('.dateFrom').val());
    }
    if ($('.dateTo').val() !== '' && $('.dateTo').val() !== undefined) {
        urlParams.set('dateTo', $('.dateTo').val());
    }
    if ($('.tenantFilter').val() !== '' && $('.tenantFilter').val() !== undefined) {
        urlParams.set('tenantFilter', $('.tenantFilter').val());
    }
    if ($('.userFilter').val() !== '' && $('.userFilter').val() !== undefined) {
        urlParams.set('userId', $('.userFilter').val());
    }
    if (statusFilter.length > 0) {
        urlParams.set('status', statusFilter);
    }
    window.location.search = urlParams.toString();
}