import {Sakura} from "./sakura.js";
// import {Falling} from "../../../../static/js/falling.js";

const sakura = new Sakura('.image--top', {
    fallSpeed: 1,
    colors: [
        {
            gradientColorStart: '#faeeea',
            gradientColorEnd: '#ca9990',
            gradientColorDegree: 120,
        },
        {
            gradientColorStart: 'rgba(90,109,3,0.62)',
            gradientColorEnd: '#0f4750',
            gradientColorDegree: 120,
        },
        {
            gradientColorStart: 'rgba(217,167,162,0.66)',
            gradientColorEnd: '#cc669b',
            gradientColorDegree: 120,
        },
        {
            gradientColorStart: '#a5405d',
            gradientColorEnd: '#eec6db',
            gradientColorDegree: 120,
        },
    ]
});

// Falling(1, 12, 'heart');
// Falling(2, 16, 'heart');
// Falling(3, 6, 'heart');

// header full color
window.addEventListener('scroll', () => {
    let distanceFromTop = window.pageYOffset
    if(distanceFromTop > 200) {
        document.querySelector('.header').classList.add('header--full')
    } else {
        document.querySelector('.header').classList.remove('header--full')
    }
})

// submit arrival
const form =  document.querySelector('.block--attend form')
const formSubmit =  document.querySelector('.block--attend input[type=submit]')
// const formSuccess =  document.querySelector('.block--attend .success')

formSubmit.addEventListener('click', (e) => {
    e.preventDefault()
    let attendees = {
        name: form.querySelector('input[type=text]').value,
        plus: form.querySelector('select:nth-child(2)').value,
        kids: form.querySelector('select:nth-child(3)').value
    }
    localStorage.setItem('attend', JSON.stringify(attendees))
    form.submit()
})
// get arrival (already sent so display name on page)
const getAttendees = JSON.parse(localStorage.getItem('attend'))
let getAttendeesName, getAttendeesPlus, getAttendeesKids;
if(getAttendees) {
    getAttendeesName = getAttendees.name
    getAttendeesPlus = getAttendees.plus
    getAttendeesKids = getAttendees.kids
}
// set attendees in form
if(getAttendeesName !== "" && getAttendeesName !== undefined) {
    form.querySelector('input[type=text]').value = getAttendeesName
    document.querySelector('.block--attend > span').textContent = 'Prijavili ste svoj dolazak'
    formSubmit.value = 'Izmeni'
}
if(getAttendeesPlus === "+1") {
    form.querySelector('select:nth-child(2)').value = "+1"
}
if(getAttendeesKids !== "Deca" && getAttendeesKids !== undefined) {
    form.querySelector('select:nth-child(3)').value = getAttendeesKids
}

// tabs
// const tabs = document.querySelectorAll('.tabs li')
// const images = document.querySelectorAll('.gallery a')
// if(tabs) {
//     tabs.forEach(item => {
//         item.addEventListener('click', (e) => {
//             // id of clicked tab
//             let id = item.dataset.tab
//
//             // set active tab
//             tabs.forEach(item => {
//                 if(item.dataset.tab === id) {
//                     item.classList.add('active')
//                 } else {
//                     item.classList.remove('active')
//                 }
//             })
//
//             // select tab images
//             images.forEach(item => {
//                 if(id === "1") {
//                     item.style.display = 'block'
//                 } else if(item.dataset.tab === id) {
//                     item.style.display = 'block'
//                 } else {
//                     item.style.display = 'none'
//                 }
//             })
//         })
//     })
// }