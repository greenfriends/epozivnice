import Page from "./Page.js";

export default class Client extends Page {

    form = 'clientForm';

    constructor() {
        super();
        this.attachSubmitEvent();
    }

    switchClientType() {
        let select = document.getElementsByClassName('clientType')[0];
        let personalIdLabel = document.getElementsByClassName('personalId')[0];
        let companyIdLabel = document.getElementsByClassName('companyId')[0];

        if (select.value == 1) {
            let vat = document.getElementsByName('pib')[0];
            vat.parentElement.hidden = false;
            this.addClass(personalIdLabel, 'none');
            this.removeClass(companyIdLabel, 'none');

            return;
        }
        if (select.value == 2) {
            let vat = document.getElementsByName('pib')[0];
            vat.parentElement.hidden = true;
            this.removeClass(personalIdLabel, 'none');
            this.addClass(companyIdLabel, 'none');
        }
    }
}