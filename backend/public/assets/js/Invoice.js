import Page from "./Page.js";

export default class Invoice extends Page {

    form = 'invoiceForm';

    constructor() {
        super();
        this.attachSubmitEvent();
        let self = this;
        document.getElementsByClassName('selectedServices')[0].addEventListener("change",function(e) {
            if (e.target && e.target.matches(".qty")) {
                self.calculateAmount();
            }
        });
        document.getElementsByClassName('selectedServices')[0].addEventListener("click",function(e) {
            if (e.target && e.target.matches(".removeService")) {
                e.target.parentNode.remove();
                self.calculateAmount();
            }
        });
    }

    loadSelectedServices() {
        let self = this;

        let selectServices = document.getElementsByClassName('selectServices')[0];
        let selectedServices = document.getElementsByClassName('selectedServices')[0];
        selectServices.addEventListener('change', async function () {
            this.disabled = true;
            let value = this.value;
            let url = "/service/getService/" + self.getSelectedOptions(this).join(',') + "/";
            self.fetchData(url, 'GET').then((services) => {
                self.createElementsForSelectedService(selectedServices, services);
                this.disabled = false;
            });
            [...this.children].forEach(function (option) {
                if (option.value === value) {
                    option.remove();
                    return;
                }
            });
            this.value = 0;
        });
    }

    fillClientInfoForInvoice() {
        let self = this;
        let invoiceClient = document.getElementsByClassName('invoiceClient')[0];
        if (invoiceClient) {
            invoiceClient.addEventListener('change', function() {
                let clientId = invoiceClient.options[invoiceClient.selectedIndex].value;
                let url = "/client/getClient/" + clientId + "/";
                self.fetchData(url, 'GET').then((client) => {
                    document.getElementsByName('clientName')[0].value = client.clientName;
                    document.getElementsByName('clientAddress')[0].value = client.clientAddress;
                    document.getElementsByName('clientCity')[0].value = client.clientCity;
                    document.getElementsByName('clientCountry')[0].value = client.clientCountry;
                    document.getElementsByName('clientVatId')[0].value = client.clientId;
                    document.getElementsByName('clientVat')[0].value = client.clientVat;
                });
            });
        }
    }

    createElementsForSelectedService(selectedServices, services) {
        let self = this;
        let amount = 0;
        services.forEach((service) => {
            let input = document.createElement('input');
            input.type = 'hidden';
            input.name = 'selectedServices[serviceId][]';
            input.value = service.serviceId;
            selectedServices.appendChild(input);

            input = document.createElement('input');
            input.type = 'hidden';
            input.name = 'selectedServices[price][]';
            input.value = service.price;
            selectedServices.appendChild(input);

            input = document.createElement('input');
            input.type = 'hidden';
            input.name = 'selectedServices[name][]';
            input.value = service.name;
            selectedServices.appendChild(input);

            input = document.createElement('input');
            input.placeholder = 'qty';
            input.className = 'qty';
            input.type = 'number';
            input.name = 'selectedServices[qty][]';
            input.value = 1;

            input.addEventListener('change', function() {
                self.calculateAmount();
            });

            let entry = document.createElement('li');
            entry.dataset.serviceId = service.serviceId;
            entry.dataset.price = service.price;
            entry.dataset.qty = 1;
            entry.className = 'selectedService';
            amount += Number(service.price);
            entry.innerHTML = service.name + '; price ' + service.price + '; qty: ';
            let tmp = document.createElement('span');
            tmp.className = 'removeService';
            tmp.innerHTML = '&nbsp; X';
            entry.appendChild(input);
            entry.appendChild(tmp);
            selectedServices.appendChild(entry);
            self.calculateAmount();
            selectedServices.getElementsByClassName('removeService')[0].addEventListener('click', function() {
                this.parentNode.remove();
            });
        });

    }

    calculateAmount() {
        let selectedServices = [...document.getElementsByClassName('selectedService')];
        let amount = 0;
        selectedServices.forEach((listItem) => {
            listItem.dataset.qty = listItem.getElementsByClassName('qty')[0].value;
            amount += Number(listItem.dataset.price * listItem.dataset.qty);
        });
        document.getElementsByName('amount')[0].value = amount;
    }
}