<?php

return [
    \Skeletor\Acl\Acl::MSG_NOT_LOGGED_IN => 'Morate biti ulogovani da bi pristupili privatnom delu.',
    \Skeletor\Acl\Acl::MSG_NO_PERMISSIONS => 'Nemate permisije da pristupite %s.',
];