<?php

date_default_timezone_set('Europe/Belgrade');

return array(
    'baseUrl' => 'https://fakture.djavolak.info',
    'appType' => 'backend',
    'timezone' => 'Europe/Belgrade',
    'adminPath' => '',
    'compileAssets' => false,
    'cropSizes' => [
        'deskSingle' => 800,
        'mobileSingle' => 400,
        'deskList' => 250,
    ],
    'middleware' => [
        0 => Skeletor\Middleware\MiddlewareInterface::class
    ],
    'imageBasePath' => IMAGES_PATH
);

