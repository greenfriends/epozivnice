<?php

/**
 * Define routes here.
 *
 * Routes follow this format:
 *
 * [METHOD, ROUTE, CALLABLE] or
 * [METHOD, ROUTE, [Class => method]]
 *
 * When controller is used without method (as string), it needs to have a magic __invoke method defined.
 *
 * Routes can use optional segments and regular expressions. See nikic/fastroute
 */
//@TODO find a proper way to use adminPath
/**
 * @var $adminPath string secret path to admin
 */
return [
    // frontend
    [['GET'], '/vencanje/{slug}/[{key}/]', \Epozivnice\Backend\Action\Event::class],
    [['GET'], '/rodjendan/{slug}/', \Epozivnice\Backend\Action\Event::class],
    [['GET'], '/pozivnice/[page/{page}/]', \Epozivnice\Backend\Action\Listing::class],
    [['GET'], '/pozivnica/{slug}/', \Epozivnice\Backend\Action\Template::class],
    [['GET'], '/pozivnica/preview/{id}/', \Epozivnice\Backend\Action\TemplatePreview::class],
    [['GET'], '/checkout/', \Epozivnice\Backend\Action\Checkout::class],
    [['GET'], '/payment/', \Epozivnice\Backend\Action\Payment::class],
    [['POST'], '/checkout-handler/', \Epozivnice\Backend\Action\CheckoutHandler::class],

    // frontend static
    [['GET'], '/', \Epozivnice\Backend\Action\Static\Index::class],
    [['GET'], '/o-nama/', \Epozivnice\Backend\Action\Static\About::class],

    // backend
    [['GET', 'POST'], '/login/{action}/', \Epozivnice\Backend\Controller\LoginController::class],
    [['GET', 'POST'], '/user/{action}/[{userId}/]', \Epozivnice\Backend\Controller\UserController::class],
    [['GET', 'POST'], '/event/{action}/[{id}/]', \Epozivnice\Backend\Controller\EventController::class],
    [['GET', 'POST'], '/template/{action}/[{id}/]', \Epozivnice\Backend\Controller\TemplateController::class],


];
