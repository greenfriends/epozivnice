<?php

$guest = [
    '/',
    '/login/loginForm/',
    '/login/login/',
    '/login/resetPassword',
    '/cron/*',
    '/vencanje/*',
    '/rodjendan/*',
    '/pozivnice/*',
    '/pozivnice/',
    '/pozivnica/*',
    '/checkout*',
    '/payment/',
    '/o-nama/',
//    '/checkout-handler/',
];

$level2 = [
    '/payment/',
    '/checkout*',
    '/pozivnice/',
    '/vencanje/*',
    '/rodjendan/*',
//    '/checkout-handler/',

    '/event/*',
    '/pozivnice/*',
    '/pozivnica/*',
    '/user/form/*',
    '/template/view/*',
    '/template/form/*',
    '/user/view/*',
    '/user/update/*',
    '/login/logout/',
];

$level1 = [
    '/cache/*',
    '/user/*',
    '/template/*',
    '/translator/*',
    '/activity/*',
];

//can also see everything level2 can see
$level1 = array_merge($level2, $level1);

return [
    0 => $guest,
    1 => $level1,
    2 => $level2
];