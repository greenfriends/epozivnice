<?php $this->layout('layout::standard') ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?=$this->t('Translations list:')?></h1>
    </div>
</div>
<div class="row biggerBottonMargin">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <!-- /.panel-heading -->
            <div class="panel-body panel-bodyWhite">
                <a href="/translator/form/" title="<?=$this->t('Create new')?>"><?=$this->t('Create new')?></a>
                <table class="table table-striped table-bordered table-hover" id="translatorTable">
                    <thead>
                    <tr>
                        <th><?=$this->t('#')?></th>
                        <th><?=$this->t('Original string')?></th>
                        <th><?=$this->t('Translated string')?></th>
                        <th><?=$this->t('Group')?></th>
                        <th><?=$this->t('Language')?></th>
                        <th><?=$this->t('Date created')?></th>
                        <th><?=$this->t('Date updated')?></th>
                        <th><?=$this->t('Action')?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    /* @var \Fakture\Translator\Model\Translator $translation */
                    foreach ($data['models'] as $translation): ?>
                    <tr>
                        <td><a href="/translator/form/<?=$translation->getId()?>/"><?=$translation->getId()?></a></td>
                        <td><?=$translation->getOriginalString()?></td>
                        <td><?=$translation->getTranslatedString()?></td>
                        <td><?=$translation->getGroup()?></td>
                        <td><?=$translation->getLanguage()?></td>
                        <td><?=$translation->getCreatedAt()->format('d/m/Y H:i')?></td>
                        <td><?=$translation->getUpdatedAt()->format('d/m/Y H:i')?></td>
                        <td><a href="/translator/delete/<?=$translation->getId()?>/" class="delete"><?=$this->t('Delete')?></a></td>
                    </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>