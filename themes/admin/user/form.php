<?php
/* @var array $data */
/* @var int $loggedInRole */
/* @var string $protectedPath */

$this->layout('layout::standard') ?>
<div class="row">
    <div class="col-lg-12">
    <?php if ($data['user']):?>
        <?php if ($loggedInRole === 1): ?>
        <h1 class="page-header"><?=$this->t('Edit user:')?> <?=$data['user']->getDisplayName()?></h1>
        <?php else: ?>
        <h1 class="page-header"><?=$this->t('Edit profile:')?></h1>
        <?php endif; ?>
    <?php else: ?>
        <h1 class="page-header"><?=$this->t('Create user:')?></h1>
    <?php endif; ?>
    </div>
</div>
<form action="/user/<?= ($data['user']) ? 'update/' . $data['user']?->getId() : 'create' ?>/"
      class="inlineForm" method="POST" enctype="multipart/form-data" id="userForm" autocomplete="off">
    <?=$this->formToken(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <h4><?=$this->t('Basic Info')?></h4>
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Email:')?></label>
                                <input name="email" value="<?=$data['user']?->getEmail() ?>" class="form-control inputDesign"
                                <?php if (!$data['user']): ?>data-validation="length" data-validation-length="min6"
                                   data-validation-error-msg="Enter email."<?php endif?> />
                            </div>

                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Display name:')?></label>
                                <input name="displayName" value="<?=$data['user']?->getDisplayName()?>" class="form-control inputDesign"
                                       <?php if (!$data['user']): ?>data-validation="length" data-validation-length="min6"
                                       data-validation-error-msg="Enter display name."<?php endif?> />
                            </div>

                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Password:')?></label>
                                <input name="password" value="" class="form-control inputDesign" type="password"
                                       <?php if (!$data['user']): ?>data-validation="length" data-validation-length="min6"
                                       data-validation-error-msg="<?=$this->t('Password:')?>"<?php endif?> />
                            </div>

                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Repeat password:')?></label>
                                <input name="password2" value="" class="form-control inputDesign" type="password"
                                       <?php if (!$data['user']): ?>data-validation="confirmation" data-validation-confirm="password" data-validation-error-msg="<?=$this->t("Password doesn't match.")?>"<?php endif?> />
                            </div>
                        </div>
                        <!-- /.col-lg-4 (nested) -->
                        <div class="col-lg-4">
                            <?php if ($loggedInRole === 1): ?>
                            <h4><?=$this->t('Additional information')?></h4>
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Role')?></label>
                                <select class="form-control selectDesign userRole" name="role" required data-validation="required"
                                        data-validation-error-msg="Select type of user.">
                                    <?php foreach (\Skeletor\User\Model\User::hrLevels() as $id => $role): ?>
                                    <option value="<?=$id?>"
                                        <?php if ($id = $data['user']?->getRole()) { echo 'selected'; }?>><?=$role?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <div class="form-group divUser">
                                <label class="userLabel"><?=$this->t('Status')?></label>
                                <select class="form-control selectDesign" name="isActive">
                                    <option value="1" <?php if ($data['user']?->getIsActive()) { echo 'selected'; }?>
                                    ><?=$this->t('Active')?></option>
                                    <option value="0" <?php if (!$data['user']?->getIsActive()) { echo 'selected'; }?>
                                    ><?=$this->t('Inactive')?></option>
                                </select>
                            </div>
                            <?php endif;?>

                            <input type="hidden" name="userId" value="<?=$data['user']?->getId()?>" />
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="row" style="float: left;">
                            <div class="form-group">
                                <input type="submit" value="<?=$this->t('Submit')?>" class="btn btn-success submitButtonSize" />
                            </div>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.row -->
    </div>
</form>