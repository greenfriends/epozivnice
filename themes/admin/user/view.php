<?php $this->layout('layout::standard') ?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?=$this->t('User management')?>:</h1>
    </div>
    <div class="col-lg-12">
        <a href="/user/form/"><?=$this->t('Create new')?></a>
    </div>
</div>
<div class="row biggerBottonMargin">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <!-- /.panel-heading -->
            <div class="row">
                <div class="col-lg-2">
                    <select class="client form-control mandatorySelect roleFilter">
                        <option value=""><?=$this->t('Role')?></option>
                        <option value="1"
                            <?php if (isset($data['filters']['role']) && ($data['filters']['role'] == 1)) {echo 'selected';} ?>><?=$this->t('Admin')?>
                        <option value="2"
                            <?php if (isset($data['filters']['role']) && ($data['filters']['role'] == 2)) {echo 'selected';} ?>><?=$this->t('User')?></option>
                    </select>
                </div>
                <div class="col-lg-2">
                    <select class="client form-control mandatorySelect statusFilter">
                        <option value=""><?=$this->t('Status')?></option>
                        <option value="0"
                            <?php if (isset($data['filters']['isActive']) && ($data['filters']['isActive'] == 0)) {echo 'selected';} ?>><?=$this->t('Inactive')?></option>
                        <option value="1"
                            <?php if (isset($data['filters']['isActive']) && ($data['filters']['isActive'] == 1)) {echo 'selected';} ?>><?=$this->t('Active')?></option>
                    </select>
                </div>
                <button class="btn btn-primary filter"><?=$this->t('Filter')?></button>
            </div>
            <div class="panel-body panel-bodyWhite">
                <table width="100%" class="table table-striped table-bordered table-hover" id="usersTable">
                    <thead>
                    <tr>
                        <th><?=$this->t('Email')?></th>
                        <th><?=$this->t('Display name')?></th>
                        <th><?=$this->t('Role')?></th>
                        <th><?=$this->t('Status')?></th>
                        <th><?=$this->t('Created at')?></th>
                        <th><?=$this->t('Updated at')?></th>
                        <th><?=$this->t('Action')?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    /* @var \Skeletor\User\Model\User $user */
                    foreach ($data['users'] as $user): ?>
                    <tr>
                        <td><a href="/user/form/<?=$user->getId()?>/"><?=$user->getEmail()?></a></td>
                        <td><?=$user->getDisplayName()?></td>
                        <td><?= \Skeletor\User\Model\User::hrLevels()[$user->getRole()]?></td>
                        <td><?= ($user->getIsActive()) ? $this->t('Active') : $this->t('Inactive') ?></td>
                        <td><?=$user->getCreatedAt()->format('d/m/Y H:i') ?></td>
                        <td><?=$user->getUpdatedAt()->format('d/m/Y H:i') ?></td>
                        <td><a href="/user/delete/<?=$user->getId()?>/" class="delete"><?=$this->t('Delete')?></a></td>
                    </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
