<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ePozivnice.rs</title>
    <meta name="description" content="Site description">

    <meta property="og:url" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="fb:app_id" content="" />
    <meta property="og:site_name" content="Site.com" />

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <meta name="theme-color" content="#FFFFFF">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="192x192" href="android-icon-192x192.png">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap" rel="stylesheet">

    <title>Elektronska pozivnica - ePozivnica.rs</title>
    <link rel="stylesheet" href="/assets/css/main.css" />
</head>
<body>
<?=$this->section('navigation', $this->fetch('partialsGlobal::navigationFront'))?>

<!--<div class="error">-->
<!--<span>-->
<!--    <svg class="icon icon--small">-->
<!--        <use href="/assets/images/sprite.svg#xmark" />-->
<!--    </svg>-->
<!--</span>-->

<?=$this->section('content')?>

<?=$this->section('navigation', $this->fetch('partialsGlobal::footer'))?>

<script data-cfasync="false" src="/assets/js/email-decode.min.js"></script>
<script async type="text/javascript" src="/assets/js/front.min.js"></script>
</body>
</html>