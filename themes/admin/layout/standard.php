<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?=$this->e($pageTitle)?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Custom fonts for this template-->
    <link href="<?=ADMIN_ASSET_URL .'/vendor/fontawesome-free/css/all.min.css'?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?=ADMIN_ASSET_URL .'/css/sb-admin-2.min.css'?>" rel="stylesheet">
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="<?=ADMIN_ASSET_URL .'/css/style.css'?>" rel="stylesheet"/>

    <?=$this->section('cssinclude')?>
    <?=$this->section('jsinclude')?>
</head>
<body id="page-top">
<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?=$this->section('navigation', $this->fetch('partialsGlobal::navigation'))?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content">
            <!-- Topbar -->
            <?=$this->section('topBar', $this->fetch('partialsGlobal::topBar'))?>
            <!-- End of Topbar -->

            <img id="loader" class="none" src="/assets/images/loader.gif" alt="loader" />
            <!-- Begin Page Content -->
            <div class="container-fluid contentWrapper">

                <!-- output flash messages -->
                <?php if (isset($messages)) {
                    echo $messages;
                } ?>

                <?=$this->section('content')?>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Spinetech <?=date('Y')?>.</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
<div id="userId" data-userId="<?=($userId ?? '')?>"></div>
<!-- Bootstrap core JavaScript-->
<script src="<?=ADMIN_ASSET_URL .'/vendor/jquery/jquery.min.js'?>"></script>
<script src="<?=ADMIN_ASSET_URL .'/vendor/bootstrap/js/bootstrap.bundle.min.js'?>"></script>
<script src="<?=ADMIN_ASSET_URL .'/js/bootstrap-datetimepicker.min.js'?>"></script>

<!-- Core plugin JavaScript-->
<script src="<?=ADMIN_ASSET_URL .'/vendor/jquery-easing/jquery.easing.min.js'?>"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>


<!-- Custom scripts for all pages-->
<script src="<?=ADMIN_ASSET_URL .'/js/sb-admin-2.js'?>"></script>
<script src="<?=ADMIN_ASSET_URL .'/js/global.js'?>" type="module"></script>
</body>
</html>