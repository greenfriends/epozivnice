<?php
/* @var array $data */
$this->layout('layout::frontend') ?>

<main>
    <div class="container">
        <section class="promo">
            <div class="promo--text">
                <h1>Profesionalne elektronske Pozivnice <br />za sve Vaše proslave</h1>
                <h2>Discover thousands of easy to customize themes, templates & CMS products, made by world-class developers.</h2>
            </div>
            <div class="promo--img">
                <img src="/assets/images/promo.webp" alt="" />
            </div>
        </section>
        <section class="category">
            <div class="category--item">
                <a href="/pozivnice/">
                    <h2>ePozivnica za Svadbe</h2>
                    <p>Preko 30 različitih pozivnica</p>
                    <img src="/assets/images/w2.jpeg" alt="ePozivnica za Svadbe" />
                </a>
            </div>
            <div class="category--item">
                <a href="/pozivnice/">
                    <h2>ePozivnica za Rođendane</h2>
                    <p>Dečiji rođendani i punoletstva</p>
                    <img src="https://www.howtogeek.com/wp-content/uploads/2021/08/birthday-cake-multicolor-sprinkles.jpg?height=200p&trim=2,2,2,2" alt="ePozivnica za Rođendane" />
                </a>
            </div>
            <div class="button">
                <a href="/pozivnice/">Vidi sve ePozivnice</a>
            </div>
        </section>
    </div>

    <section class="popular">
        <div class="container">
            <div class="popular--top">
                <h3>Najpopularnije ePozivnice</h3>
                <p>We carefully review new entries from our community one by one to make sure they meet high-quality design</p>
                <ul>
                    <li class="active"><a href="/pozivnice/">Sve pozivnice</a></li>
                    <li><a href="">Svadbe</a></li>
                    <li><a href="">Dečiji rođendani</a></li>
                    <li><a href="">Bebi rođendani</a></li>
                </ul>
            </div>
            <div class="list--items">
                <?php foreach ($data['templates'] as $template): ?>
                    <div class="list--item">
                        <a href="/pozivnica/<?=$template->getSlug()?>/">
                            <img src="<?=$template->getImage('mobileSingle')?>" alt="<?=$template->getLabel()?>" />
                            <h3><?=$template->getLabel()?></h3>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="button">
                <a href="/pozivnice/">Pogledaj još</a>
            </div>
        </div>
    </section>


    <section class="works">
        <div class="container">
            <h3>Kako poručiti ePozivnicu</h3>
            <div class="works--item">
                <span>1</span>
                <p>Izaberi pozivnicu koja ti najviše odgovara</p>
                <img src="/assets/images/w2.jpeg" alt="" />
            </div>
            <div class="works--item">
                <span>2</span>
                <p>Unesi slike i podatke za tvoju proslavu</p>
                <img src="/assets/images/w2.jpeg" alt="" />
            </div>
            <div class="works--item">
                <span>3</span>
                <p>Izvrši plaćanje i prosledi link prijateljima</p>
                <img src="/assets/images/w2.jpeg" alt="" />
            </div>
            <div class="button">
                <a href="/pozivnice/">Poruči ePozivnicu</a>
            </div>
        </div>
    </section>
</main>