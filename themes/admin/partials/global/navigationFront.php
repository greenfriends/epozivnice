<header class="header">
    <div class="container">
        <div class="header--logo">
            <a href="/" title="ePozivnica">
                <span>
                    <svg class="icon icon--small">
                        <use href="static/build/sprite.svg#party-horn" />
                    </svg>
                </span>
                ePozivnica
            </a>
        </div>
        <nav>
            <ul>
                <li><a href="/pozivnice/">Sve ePozivnice</a></li>
                <li><a href="/how-to/">Kako poručiti</a></li>
                <li><a href="/kontakt/">Kontakt</a></li>
            </ul>
        </nav>
        <!--            <div class="header&#45;&#45;icons">-->
        <!--                <div class="header&#45;&#45;login">-->
        <!--                    <a href="">-->
        <!--                        <svg class="icon">-->
        <!--                            <use href="static/build/sprite.svg#smile" />-->
        <!--                        </svg>-->
        <!--                        Uloguj se-->
        <!--                    </a>-->
        <!--                </div>-->
        <!--                <div>-->
        <!--                    <a href="">-->
        <!--                        <svg class="icon">-->
        <!--                            <use href="static/build/sprite.svg#wishlist" />-->
        <!--                        </svg>-->
        <!--                    </a>-->
        <!--                </div>-->
        <!--                <div>-->
        <!--                    <a href="">-->
        <!--                        <svg class="icon">-->
        <!--                            <use href="static/build/sprite.svg#cart" />-->
        <!--                        </svg>-->
        <!--                    </a>-->
        <!--                </div>-->
        <!--            </div>-->
        <div class="header--button">
            <div class="button button--secondary">
                <a href="/pozivnice/">
                    Poruči
                    <svg class="icon icon--small">
                        <use href="static/build/sprite.svg#arrow-right-long" />
                    </svg>
                </a>
            </div>
        </div>
    </div>
</header>