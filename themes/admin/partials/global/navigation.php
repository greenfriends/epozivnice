<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-text mx-3">ePozivnice</div>
    </a>

<?php if($loggedIn): ?>
    <!-- Heading -->
    <div class="sidebar-heading"><?=$this->t('Dashboard')?></div>

    <li class="nav-item">
        <a class="nav-link collapsed" href="/event/view/">
            <i class="fas fa-fw fa-cog"></i>
            <span><?=$this->t('Events')?></span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="/template/view/">
            <i class="fas fa-fw fa-cog"></i>
            <span><?=$this->t('Templates')?></span>
        </a>
    </li>

    <hr class="sidebar-divider">

    <!-- Nav Item - Pages Collapse Menu -->
    <?php if($loggedInRole === \Skeletor\User\Model\User::ROLE_ADMIN): ?>
    <li class="nav-item">
        <a class="nav-link collapsed" href="/user/view/">
            <i class="fas fa-fw fa-cog"></i>
            <span><?=$this->t('Users')?></span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="/translator/view/">
            <i class="fas fa-fw fa-cog"></i>
            <span><?=$this->t('Translations')?></span>
        </a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">
    <?php endif; ?>

    <!-- Divider -->
    <hr class="sidebar-divider">
<?php endif; ?>

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>