<section class="custom">
    <div class="container">
        <h3>Hoćete UNIKATNU pozivnicu? Koju niko drugi nema? <span>Može!</span></h3>
        <p>Kontaktirajte nas na: <a href="mailto:kontaktiraj@gmail.com">dodati mail</a></p>
    </div>
</section>

<footer class="footer">
    <div class="container container--main">
        <div class="footer--list">
            <h4>© <?=date('Y')?> ePozivnica</h4>
            <ul>
                <li><a href="/uslovi/">Terms & Conditions</a></li>
                <li><a href="/privatnost/">Privacy Policy</a></li>
            </ul>
        </div>
        <div class="footer--list">
            <h4>O nama</h4>
            <ul>
                <li><a href="/o-nama/">Naša priča</a></li>
                <li><a href="/kontakt/">Kontakt</a></li>
            </ul>
        </div>
        <div class="footer--list">
            <h4>Info</h4>
            <ul>
                <li><a href="/howitworks/">Kako poručiti</a></li>
                <li><a href="/faq/">FAQs</a></li>
            </ul>
        </div>
        <div class="footer--list">
            <h4>Zapratite nas</h4>
            <ul class="footer--social">
                <li><a href="" class="instagram" title="Instagram">
                        <svg class="icon">
                            <use href="/assets/images/sprite.svg#instagram"></use>
                        </svg>
                    </a></li>
                <li><a href="" class="facebook" title="Facebook">
                        <svg class="icon">
                            <use href="/assets/images/sprite.svg#facebook-f"></use>
                        </svg>
                    </a></li>
                <li><a href="" class="whatsapp" title="Whatsapp">
                        <svg class="icon">
                            <use href="/assets/images/sprite.svg#whatsapp"></use>
                        </svg>
                    </a></li>
            </ul>
        </div>
    </div>
</footer>