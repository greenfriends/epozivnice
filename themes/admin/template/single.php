<?php
/* @var array $data */
/* @var int $loggedInRole */
/* @var string $protectedPath */
/* @var Epozivnice\Template\Model\Template $template */

$this->layout('layout::frontend') ?>

<main>
    <div class="container">
        <article class="details">
            <div class="details--image">
                <iframe id="preview" width="100%" height="100%" src="/pozivnica/preview/<?=$data['template']->getId()?>/"></iframe>
            </div>
            <div class="details--text">
            <form action="/checkout/">
                <h1><?=$data['template']->getLabel()?></h1>
                <p><?=$data['template']->getDescription()?></p>
                <div class="details--color">
                    <h2>Boja</h2>
                    <ul>
                        <?php
                        $active = 'active';
                        $defaultColor = '';
                        $i = 0;
                        foreach($data['template']->getFilters('colors') as $class => $label):
                            if ($i === 0) {
                                $defaultColor = $class;
                                $i++;
                            }
                        ?>
                            <li class="<?=$active?>"><span class="<?=$class?>"></span></li>
                            <?php $active = '';
                        endforeach; ?>
                    </ul>
                    <input type="hidden" name="templateId" value="<?=$data['template']->getId()?>" />
                    <input type="hidden" name="color" id="color" value="<?=$defaultColor?>" />
                </div>
                <div class="details--price">
                    <h2>Cena: <?=$data['template']->getPrice()?> din* <strong>xxx din</strong></h2>
                    <i>* Cena sa popustom od 50% do 1. maja</i>
                </div>
                <div class="details--buttons">
                    <div class="button button--secondary">
                        <button type="submit" class="">Izaberi / Vidi prikaz</button>
                    </div>
                </div>
                <div class="details--list desktop">
                    <h3>Kupovinom ePozivnice dobijate:</h3>
                    <ul>
                        <li>
                            <span class="circle">
                                <svg class="icon icon--small">
                                    <use href="/assets/images/sprite.svg#check" />
                                </svg>
                            </span>
                            <div>ePozivnicu koju šaljete prijateljima na email, viber, whatsapp...</div>
                        </li>
                        <li>
                            <span class="circle">
                                <svg class="icon icon--small">
                                    <use href="/assets/images/sprite.svg#check" />
                                </svg>
                            </span>
                            <div>ePozivnica traje do dana Vaše proslave</div>
                        </li>
                        <li>
                            <span class="circle">
                                <svg class="icon icon--small">
                                    <use href="/assets/images/sprite.svg#check" />
                                </svg>
                            </span>
                            <div>Evidenciju gostiju ko dolazi na Vašu proslavu</div>
                        </li>
                        <li>
                            <span class="circle">
                                <svg class="icon icon--small">
                                    <use href="/assets/images/sprite.svg#check" />
                                </svg>
                            </span>
                            <div>Cena: <strong>do 1. maja BESPLATNO</strong> <span class="through">xxx din </span></div>
                        </li>
                    </ul>
                </div>
            </form>
            </div>
            <div class="details--list mobile">
                <h3>Kupovinom ePozivnice dobijate:</h3>
                <ul>
                    <li>
                        <span class="circle">
                            <svg class="icon icon--small">
                                <use href="/assets/images/sprite.svg#check" />
                            </svg>
                        </span>
                        <div>ePozivnicu koju šaljete prijateljima na email, viber, whatsapp...</div>
                    </li>
                    <li>
                        <span class="circle">
                            <svg class="icon icon--small">
                                <use href="/assets/images/sprite.svg#check" />
                            </svg>
                        </span>
                        <div>ePozivnica traje do dana Vaše proslave</div>
                    </li>
                    <li>
                        <span class="circle">
                            <svg class="icon icon--small">
                                <use href="/assets/images/sprite.svg#check" />
                            </svg>
                        </span>
                        <div>Evidenciju gostiju ko dolazi na Vašu proslavu</div>
                    </li>
                    <li>
                        <span class="circle">
                            <svg class="icon icon--small">
                                <use href="/assets/images/sprite.svg#check" />
                            </svg>
                        </span>
                        <div>Cena: <strong>do 1. maja BESPLATNO</strong> <span class="through">6.000 din </span></div>
                    </li>
                </ul>
            </div>
        </article>
    </div>
    <aside class="view-more">
        <div class="container">
            <h2>Najtraženije ePozivnice</h2>
            <div class="list--items">
            <?php foreach ($data['templates'] as $template): ?>
                <div class="list--item">
                    <a href="/pozivnica/<?=$template->getSlug()?>/">
                        <img src="<?=$template->getImage('mobileSingle')?>" alt="<?=$template->getLabel()?>" />
                        <h3><?=$template->getLabel()?></h3>
                    </a>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </aside>
    <div class="to-top">
        <span>Top</span>
        <svg class="icon">
            <use href="/assets/images/sprite.svg#angle-up" />
        </svg>
    </div>
</main>