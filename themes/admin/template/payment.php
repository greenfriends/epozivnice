<?php
/* @var array $data */
/* @var int $loggedInRole */
/* @var string $protectedPath */
/* @var Epozivnice\Template\Model\Template $template */

$this->layout('layout::frontend') ?>

<main>
    <div class="order--top order--step2">
        <div class="container">
            <ul>
                <li>
                    <div>
                        <span>1</span>
                        <svg class="icon icon--small"><use href="/assets/images/sprite.svg#check" /></svg>
                    </div>
                    <span>Unos</span>
                    <svg class="icon"><use href="/assets/images/sprite.svg#arrow-right-super-long" /></svg>
                </li>
                <li>
                    <div>
                        <span>2</span>
                        <svg class="icon"><use href="/assets/images/sprite.svg#check" /></svg>
                    </div>
                    <span>Plaćanje</span>
                    <svg class="icon"><use href="/assets/images/sprite.svg#arrow-right-super-long" /></svg>
                </li>
                <li>
                    <div>
                        <span>3</span>
                        <svg class="icon"><use href="/assets/images/sprite.svg#check" /></svg>
                    </div>
                    <span>Aktivacija</span>
                </li>
            </ul>
        </div>
    </div>
    <div class="container">
        <section class="order">
            <div class="success">
                <span>
                    <svg class="icon icon--small">
                        <use href="/assets/images/sprite.svg#check" />
                    </svg>
                </span>
                Pozivnica je uspešno napravljena i biće aktivirana nakon uplate.
            </div>
            <div class="order--payment">
                <h1>Napravite uplatu: <?=$data['template']->getPrice()?> din <span class="payment--no">br. Vaše pozivnice <span><?=$data['event']->getId()?></span></span></h1>
                <p>Izaberite način plaćanja i napravite uplatu</p>
                <div>
                    <h2>Na račun:</h2>
                    <div class="payment--bank">
                        <ul>
                            <li>
                                <span>Račun:</span>
                                <span>160-602-16</span>
                            </li>
                            <li>
                                <span>Poziv na broj:</span>
                                <span><?=$data['event']->getId()?></span>
                            </li>
                            <li>
                                <span>Primalac:</span>
                                <span>xxx</span>
                            </li>
                        </ul>
                    </div>
                    <img src="/assets/images/uplatnica.webp" alt="Uplatnica ePozivnica" />
                </div>
                <div>
                    <h2>Paypal:</h2>
                    <div class="payment--paypal">
                        <div id="smart-button-container">
                            <div style="text-align: center;">
                                <div id="paypal-button-container"></div>
                            </div>
                        </div>
                        <script src="https://www.paypal.com/sdk/js?client-id=sb&enable-funding=venmo&currency=EUR" data-sdk-integration-source="button-factory"></script>
                        <script>
                            const paymentNo = document.querySelector('.payment--no span').innerHTML
                            const paymentText = `Broj pozivnice: ${paymentNo}`
                            function initPayPalButton() {
                                paypal.Buttons({
                                    style: {
                                        shape: 'rect',
                                        color: 'gold',
                                        layout: 'horizontal',
                                        label: 'pay',

                                    },

                                    createOrder: function(data, actions) {
                                        return actions.order.create({
                                            purchase_units: [{"description":paymentText,"amount":{"currency_code":"EUR","value":25}}]
                                        });
                                    },

                                    onApprove: function(data, actions) {
                                        return actions.order.capture().then(function(orderData) {

                                            // Full available details
                                            console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));

                                            // Show a success message within this page, e.g.
                                            const element = document.getElementById('paypal-button-container');
                                            element.innerHTML = '';
                                            element.innerHTML = '<h3>Hvala na uplati!</h3>';

                                            // Or go to another URL:  actions.redirect('thank_you.html');

                                        });
                                    },

                                    onError: function(err) {
                                        console.log(err);
                                    }
                                }).render('#paypal-button-container');
                            }
                            initPayPalButton();
                        </script>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="to-top">
        <span>Top</span>
        <svg class="icon">
            <use href="/assets/images/sprite.svg#angle-up" />
        </svg>
    </div>
</main>