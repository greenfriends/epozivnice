<?php
/* @var \Epozivnice\Template\Model\Template $template */
$this->layout('layout::standard');
if ($data['errors']) {
    echo $data['errors'];
}
?>
<div class="row">
    <div class="col-lg-12">
        <?php if ($data['model']):
            $templateId = $data['model']->getId(); ?>
            <h1 class="page-header"><?=$this->t('Edit template:')?> <?=$data['model']->getLabel()?></h1>
        <?php else:
            $templateId = 'null'; ?>
            <h1 class="page-header"><?=$this->t('Create template')?></h1>
        <?php endif;?>
    </div>
</div>
<form action="/template/<?= ($data['model']) ? 'update/' . $templateId : 'create' ?>/"
      class="inlineForm" method="POST" enctype="multipart/form-data" id="templateForm" autocomplete="off">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="userLabel"><?=$this->t('Tip proslave')?></label>
                                <?php if (!$data['model']?->getId()): ?>
                                <select class="form-control" name="type">
                                    <option <?php if ($data['model']?->getType() == 1) { echo 'selected'; } ?> value="1">Venčanje</option>
                                    <option <?php if ($data['model']?->getType() == 2) { echo 'selected'; } ?> value="2">Rođendan</option>
                                </select>
                                <?php else: ?>
                                <p class="inputDesign form-control"><?=\Epozivnice\Template\Model\Template::getHrType($data['model']?->getType())?></p>
                                <input type="hidden" name="type" value="<?=$data['model']?->getType()?>" />
                                <?php endif; ?>
                            </div>

                            <div class="form-group ">
                                <label class="userLabel"><?=$this->t('Naziv')?></label>
                                <input name="label" value="<?=$data['model']?->getLabel() ?>" class="form-control inputDesign" />
                            </div>

                            <div class="form-group ">
                                <label class="userLabel"><?=$this->t('Naziv templejt fajla')?></label>
                                <input name="templateFile" value="<?=$data['model']?->getTemplateFile()?>" class="form-control inputTemplate" />
                            </div>

                            <div class="form-group ">
                                <label class="userLabel"><?=$this->t('Naziv css fajla')?></label>
                                <input name="cssFile" value="<?=$data['model']?->getCssFile()?>" class="form-control inputTemplate" />
                            </div>

                            <div class="form-group ">
                                <label class="userLabel"><?=$this->t('Naziv js fajla (opciono)')?></label>
                                <input name="jsFile" value="<?=$data['model']?->getJsFile()?>" class="form-control inputTemplate" />
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group ">
                                <label class="userLabel"><?=$this->t('Cena')?></label>
                                <input name="price" value="<?=$data['model']?->getPrice() ?>" class="form-control inputDesign" />
                            </div>

                            <div class="form-group ">
                                <label class="userLabel"><?=$this->t('Opis')?></label>
                                <textarea name="description" class="form-control inputDesign"><?=$data['model']?->getDescription() ?></textarea>
                            </div>

                            <div class="form-group ">
                                <label class="userLabel"><?=$this->t('Dostupne boje')?></label>
                                <div>
                                <?php $selectedColors = [];
                                if ($data['model']) {
                                    $selectedColors = array_keys($data['model']->getFilters('colors'));
                                } ?>
                                <?php foreach(\Epozivnice\Template\Model\Color::$colors as $class => $label): ?>
                                <input type="checkbox" name="colors[<?=$class?>]" id="<?=$class?>" <?php
                                if (in_array($class, $selectedColors)) { echo 'checked'; } ?> />
                                <label for="<?=$class?>" ><?=$label?></label>
                                <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group ">
                                <label class="userLabel"><?=$this->t('Slika')?></label>
                                <input name="image" type="file" value="<?=$data['model']?->getImage()?>" class="form-control inputTemplate" />
                                <?php if ($data['model']): ?>
                                    <img width="40%" src="<?=$data['model']->getImage()?>" />
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="row">
                            <div class="form-group">
                                <input type="hidden" name="templateId" value="<?=$templateId?>" />
                                <input type="submit" value="<?=$this->t('Save')?>" class="btn btn-success submitButtonSize" id="submitButton" />
                            </div>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.row -->
    </div>
</form>
