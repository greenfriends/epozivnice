<?php
/* @var array $data */
/* @var int $loggedInRole */
/* @var string $protectedPath */
/* @var Epozivnice\Template\Model\Template $template */
$this->layout('layout::frontend') ?>
<main>
    <div class="order--top order--step1">
        <div class="container">
            <ul>
                <li>
                    <div>
                        <span>1</span>
                        <svg class="icon icon--small">
                            <use href="/assets/images/sprite.svg#check" />
                        </svg>
                    </div>
                    <span>Unos</span>
                    <svg class="icon">
                        <use href="/assets/images/sprite.svg#arrow-right-super-long" />
                    </svg>
                </li>
                <li>
                    <div>
                        <span>2</span>
                        <svg class="icon">
                            <use href="/assets/images/sprite.svg#check" />
                        </svg>
                    </div>
                    <span>Plaćanje</span>
                    <svg class="icon">
                        <use href="/assets/images/sprite.svg#arrow-right-super-long" />
                    </svg>
                </li>
                <li>
                    <div>
                        <span>3</span>
                        <svg class="icon">
                            <use href="/assets/images/sprite.svg#check" />
                        </svg>
                    </div>
                    <span>Aktivacija</span>
                </li>
            </ul>
        </div>
    </div>
    <div class="container">
        <section class="order">
            <div class="order--entry">
                <h1>Unos podataka i slika</h1>
                <p>Unesite tražene podatke za Vašu proslavu. </p>
                <form action="/checkout-handler/" method="post" enctype="multipart/form-data">
                    <?=$this->formToken(); ?>
                    <?php if ($data['errors']): ?>
                    <div class="error">
                        <span>
                            <svg class="icon icon--small">
                                <use href="/assets/images/sprite.svg#xmark" />
                            </svg>
                        </span>Imate greške u formularu, ispravite i probajte ponovo.
                        <?php if ($data['errors']) :
                            foreach ($data['errors'] as $errors):
                                foreach ($errors as $error):?>
                        <span>
                            <svg class="icon icon--small">
                                <use href="/assets/images/sprite.svg#xmark" />
                            </svg>
                        </span><?=$error?>
                        <?php   endforeach;
                            endforeach;
                        endif;?>
                    </div>
                    <?php endif; ?>
                    <div>
                        <h2>Boja</h2>
                        <div class="details--color">
                            <ul>
                                <li class="active"><span class="white"></span></li>
                                <li><span class="gold"></span></li>
                                <li><span class="rose-gold"></span></li>
                            </ul>
                            <input type="hidden" name="color" value="white" />
                            <input type="hidden" name="type" value="<?=$data['template']->getType()?>" />
                            <input type="hidden" name="templateId" value="<?=$data['template']->getId()?>" />
                        </div>
                    </div>
                    <div>
                        <h2>O slavljeniku/ci</h2>
                        <input name="host1FirstName" type="text" placeholder="Ime slavljenika/ce" required="required"
                            <?php if (isset($data['formData']['host1FirstName'])) { echo 'value="'.$data['formData']['host1FirstName'].'"'; } ?> />
                        <input name="host1LastName" type="text" placeholder="Koliko puni godina?" required="required"
                            <?php if (isset($data['formData']['host1LastName'])) { echo 'value="'.$data['formData']['host1LastName'].'"'; } ?> />
                    </div>
                    <div>
                        <h2>Slika</h2>
                        <div class="order--image">
                            <div>
                                <svg class="icon"><use href="/assets/images/sprite.svg#people" /></svg>
                                <span><svg class="icon"><use href="/assets/images/sprite.svg#camera" /></svg></span>
                            </div>
                            <input name="image1" class="order--file" type="file" required />
                        </div>
                    </div>
                    <div>
                        <h2>Datum proslave</h2>
                        <div class="entry--date">
                            <input type="number" maxlength="2" placeholder="Dan" required name="eventDate[day]" min="1" step="1" max="31"
                                   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                            />
                            <input type="number" maxlength="2" placeholder="Mesec" required name="eventDate[month]" min="1" step="1" max="12"
                                   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                            />
                            <input type="number" maxlength="4" placeholder="Godina" required name="eventDate[year]" min="<?=(int)date('Y')?>" step="1" max="<?=(int)date('Y')+3?>"
                                   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                            />
                        </div>
                        <h2>Vreme proslave</h2>
                        <div class="entry--date">
                            <input type="number" maxlength="2" placeholder="Sati" required name="eventDate[hour]" min="1" step="1" max="23"
                                   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                            />
                            <input type="number" maxlength="2" placeholder="Minuta" required name="eventDate[minute]" min="0" step="15" max="59"
                                   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                            />
                        </div>
                    </div>
                    <div>
                        <h2>Lokacija proslave</h2>
                        <input name="locationName" type="text" placeholder="Naziv objekta" required />
                        <input name="locationCity" type="text" placeholder="Grad" required />
                        <input name="locationAddress" class="full-width" type="text" placeholder="Adresa" required />
                        <textarea name="description" placeholder="Opis (Opciono)" rows="4"></textarea>
                    </div>
                    <div>
                        <h2>Vaši podaci</h2>
                        <i>Ove informacije neće biti vidljive na ePozivnici</i>
                        <input name="userName" class="full-width" type="text" placeholder="Ime i prezime" required
                        <?php if (isset($data['formData']['userName'])) { echo 'value="'.$data['formData']['userName'].'"'; } ?> />
                        <input name="email" class="full-width" type="text" placeholder="Email" required
                        <?php if (isset($data['formData']['email'])) { echo 'value="'.$data['formData']['email'].'"'; } ?> />
                        <input name="phone" class="full-width" type="number" placeholder="Telefon" required
                        <?php if (isset($data['formData']['phone'])) { echo 'value="'.$data['formData']['phone'].'"'; } ?> />
                    </div>
                    <div class="button">
                        <input type="submit" value="Pošalji" onClick="highlightRequired()" />
                    </div>
                </form>
            </div>
            <div class="order--preview">
                <img src="<?=$data['template']->getImage()?>" alt="<?=$data['template']->getLabel()?>" />
            </div>
        </section>
    </div>
    <div class="to-top">
        <span>Top</span>
        <svg class="icon">
            <use href="/assets/images/sprite.svg#angle-up" />
        </svg>
    </div>
</main>