<?php
/* @var array $data */
/* @var int $loggedInRole */
/* @var string $protectedPath */
/* @var Epozivnice\Template\Model\Template $template */

$this->layout('layout::frontend') ?>

<main>
    <div class="container">
        <h1 class="list--title">Pozivnice za Svadbe</h1>

        <section class="list">
            <div class="list--left">
                <div class="close--filters">
                    <span class="flex-space"></span>
                    <span class="close--filters--button">Filteri</span>
                    <svg class="icon">
                        <use href="/assets/images/sprite.svg#xmark"></use>
                    </svg>
                </div>
                <div class="filters--scroll">
                    <h4>Kategorije</h4>
                    <?php
                    $wedding = '';
                    $birthday = '';
                    if (isset($data['filter']['type'])) {
                        if ($data['filter']['type'] == 1) {
                            $wedding = 'active';
                        }
                        if ($data['filter']['type'] == 2) {
                            $birthday = 'active';
                        }
                    }?>
                    <ul>
                        <li class="<?=$wedding?>"><a href="/pozivnice/?category=1">Svadbe <span><?=$data['counts']['weddingCount']?></span></a></li>
                        <li class="<?=$birthday?>"><a href="/pozivnice/?category=2">Rođendani <span><?=$data['counts']['birthdayCount']?></span></a></li>
                    </ul>
<!--                    <h4>Stilovi</h4>-->
<!--                    <div class="list--style">-->
<!--                        <div><span><svg class="icon icon--small">-->
<!--                            <use href="/assets/images/sprite.svg#check" />-->
<!--                        </svg></span>Elegant</div>-->
<!--                        <div><span><svg class="icon icon--small">-->
<!--                            <use href="/assets/images/sprite.svg#check" />-->
<!--                        </svg></span>Klasik</div>-->
<!--                    </div>-->

                    <h4>Boje</h4>
                    <div class="list--style">
                        <?php foreach(\Epozivnice\Template\Model\Color::$colors as $class => $label): ?>
                        <div>
                            <span>
                                <svg class="icon icon--small"><use href="/assets/images/sprite.svg#check" /></svg>
                            </span>
                            <div class="list--color <?=$class?>"></div><?=$label?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="list--right">
                <div class="list--sort-filter">
                    <div class="open--filters">
                        <svg class="icon">
                            <use href="/assets/images/sprite.svg#sliders-up" />
                        </svg>
                        Filteri (2)
                    </div>
                    <div class="list--sort">
                        <label for="sorting">
                            <svg class="icon"><use href="/assets/images/sprite.svg#arrow-up-arrow-down" /></svg>
                            <span>Sortiraj</span>
                        </label>
                        <select id="sorting" onChange="getSorting(this);">
                            <option value="1">Najnovije</option>
                            <option value="2">Najpopulanije</option>
                        </select>
                    </div>
                </div>
                <div class="list--items">
                    <?php foreach ($data['templates'] as $template): ?>
                    <div class="list--item">
                        <a href="/pozivnica/<?=$template->getSlug()?>/">
                            <img src="<?=$template->getImage()?>" alt="<?=$template->getLabel()?>" />
                            <h3><?=$template->getLabel()?></h3>
                        </a>
                        <div class="details--color">
                            <ul>
                            <?php
                            foreach($template->getFilters('colors') as $class => $label): ?>
                                <li class="noJs"><span class="<?=$class?>"></span></li>
                            <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <?php endforeach;?>
                    <div class="list--loading">
                        <div>
                            <svg class="icon icon--medium">
                                <use href="/assets/images/sprite.svg#spinner" />
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>