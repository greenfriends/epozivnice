<?php
/* @var array $data */
/* @var int $loggedInRole */
/* @var string $protectedPath */
/* @var Epozivnice\Template\Model\Template $template */

$this->layout('layout::frontend') ?>

<main>
    <div class="order--top order--step1">
        <div class="container">
            <ul>
                <li>
                    <div>
                        <span>1</span>
                        <svg class="icon icon--small">
                            <use href="/assets/images/sprite.svg#check" />
                        </svg>
                    </div>
                    <span>Unos</span>
                    <svg class="icon">
                        <use href="/assets/images/sprite.svg#arrow-right-super-long" />
                    </svg>
                </li>
                <li>
                    <div>
                        <span>2</span>
                        <svg class="icon">
                            <use href="/assets/images/sprite.svg#check" />
                        </svg>
                    </div>
                    <span>Plaćanje</span>
                    <svg class="icon">
                        <use href="/assets/images/sprite.svg#arrow-right-super-long" />
                    </svg>
                </li>
                <li>
                    <div>
                        <span>3</span>
                        <svg class="icon">
                            <use href="/assets/images/sprite.svg#check" />
                        </svg>
                    </div>
                    <span>Aktivacija</span>
                </li>
            </ul>
        </div>
    </div>
    <div class="container">
        <section class="order">
            <div class="order--entry">
                <h1>Unos podataka i slika</h1>
                <p>Unesite tražene podatke za Vašu proslavu. </p>
                <form action="/checkout-handler/" method="post" enctype="multipart/form-data">
                    <?=$this->formToken(); ?>
                    <?php if ($data['errors']): ?>
                    <div class="error">
                        <span>
                            <svg class="icon icon--small">
                                <use href="/assets/images/sprite.svg#xmark" />
                            </svg>
                        </span>Imate greške u formularu, ispravite i probajte ponovo.
                        <?php if ($data['errors']) :
                            foreach ($data['errors'] as $errors):
                                foreach ($errors as $error):?>
                        <span>
                            <svg class="icon icon--small">
                                <use href="/assets/images/sprite.svg#xmark" />
                            </svg>
                        </span><?=$error?>
                        <?php   endforeach;
                            endforeach;
                        endif;?>
                    </div>
                    <?php endif; ?>
                    <div>
                        <h2>Boja</h2>
                        <div class="details--color">
                            <ul>
                                <?php
                                $active = 'active';
                                $defaultColor = '';
                                $i = 0;
                                foreach($data['template']->getFilters('colors') as $class => $label):
                                    if ($i === 0) {
                                        $defaultColor = $class;
                                        $i++;
                                    }
                                    ?>
                                    <li class="<?=$active?>"><span class="<?=$class?>"></span></li>
                                    <?php $active = '';
                                endforeach; ?>
                            </ul>
                            <input type="hidden" name="color" id="color" value="<?=$defaultColor?>" />
                            <input type="hidden" name="type" value="<?=$data['template']->getType()?>" />
                            <input type="hidden" name="templateId" value="<?=$data['template']->getId()?>" />
                        </div>
                    </div>
                    <div>
                        <h2>Ime i prezime mladenaca</h2>
                        <input name="host1FirstName" type="text" placeholder="Ime mlade" required="required"
                            <?php if (isset($data['formData']['host1FirstName'])) { echo 'value="'.$data['formData']['host1FirstName'].'"'; } ?> />
                        <input name="host1LastName" type="text" placeholder="Prezime mlade" required="required"
                            <?php if (isset($data['formData']['host1LastName'])) { echo 'value="'.$data['formData']['host1LastName'].'"'; } ?> />
                        <input name="host2FirstName" type="text" placeholder="Ime mladoženje" required="required"
                            <?php if (isset($data['formData']['host2FirstName'])) { echo 'value="'.$data['formData']['host2FirstName'].'"'; } ?> />
                        <input name="host2LastName" type="text" placeholder="Prezime mladoženje" required="required"
                            <?php if (isset($data['formData']['host2LastName'])) { echo 'value="'.$data['formData']['host2LastName'].'"'; } ?> />
                    </div>
                    <div>
                        <h2>Slika na vrhu</h2>
                        <div class="order--image">
                            <div>
                                <svg class="icon"><use href="/assets/images/sprite.svg#people" /></svg>
                                <span><svg class="icon"><use href="/assets/images/sprite.svg#camera" /></svg></span>
                            </div>
                            <input type="file" class="order--file" name="image1" required id="image1" accept=".jpg, .jpeg, .png" />
                        </div>
                    </div>
<!--                    <div>-->
<!--                        <h2>Slika na dnu</h2>-->
<!--                        <div class="order--image">-->
<!--                            <div>-->
<!--                                <svg class="icon"><use href="/assets/images/sprite.svg#people" /></svg>-->
<!--                                <span><svg class="icon"><use href="/assets/images/sprite.svg#camera" /></svg></span>-->
<!--                            </div>-->
<!--                            <input name="image2" class="order--file" type="file" />-->
<!--                        </div>-->
<!--                    </div>-->
                    <div>
                        <h2>Datum svadbe</h2>
                        <div class="entry--date">
                            <input type="number" maxlength="2" placeholder="Dan" required name="eventDate[day]" min="1" step="1" max="31"
                                   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                            />
                            <input type="number" maxlength="2" placeholder="Mesec" required name="eventDate[month]" min="1" step="1" max="12"
                                   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                            />
                            <input type="number" maxlength="4" placeholder="Godina" required name="eventDate[year]" min="<?=(int)date('Y')?>" step="1" max="<?=(int)date('Y')+3?>"
                                   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                            />
                        </div>
                        <h2>Vreme svadbe</h2>
                        <div class="entry--date">
                            <input type="number" maxlength="2" placeholder="Sati" required name="eventDate[hour]" min="1" step="1" max="23"
                                   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                            />
                            <input type="number" maxlength="2" placeholder="Minuta" required name="eventDate[minute]" min="0" step="15" max="59"
                                   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                            />
                        </div>
                    </div>
                    <div>
                        <h2>Lokacija proslave</h2>
                        <input name="locationName" type="text" placeholder="Naziv objekta" required />
                        <input name="locationCity" type="text" placeholder="Grad" required />
                        <input name="locationAddress" class="full-width" type="text" placeholder="Adresa" required />
                        <textarea name="description" placeholder="Opis (Opciono)" rows="4"></textarea>
                    </div>
                    <div>
                        <h2>O nama (opciono)</h2>

                        <div class="entry--wrapper">
                            <textarea placeholder="Kako smo se upoznali" rows="4" name="aboutHosts[description][]"></textarea>
                            <div class="entry--date">
                                <input name="aboutHosts[day][]" type="number" maxlength="2" placeholder="Dan"
                                   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                />
                                <input name="aboutHosts[month][]" type="number" maxlength="2" placeholder="Mesec"
                                    oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                />
                                <input name="aboutHosts[year][]" type="number" maxlength="4" placeholder="Godina"
                                    oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                />
                            </div>
                        </div>
                        <div class="entry--wrapper">
                            <textarea placeholder="Prosidba" rows="4"></textarea>
                            <div class="entry--date">
                                <input name="aboutHosts[day][]" type="number" maxlength="2" placeholder="Dan"
                                   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                />
                                <input name="aboutHosts[month][]" type="number" maxlength="2" placeholder="Mesec"
                                   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                />
                                <input name="aboutHosts[year][]" type="number" maxlength="4" placeholder="Godina"
                                   oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                />
                            </div>
                        </div>
                    </div>
                    <div>
                        <h2>Galerija slika (opciono)</h2>
                        <div class="entry--gallery">
                            <label for="files--us">Izaberite slike:</label>
                            <input type="file" id="files--us" name="files--us" multiple>
                            <ul class="entry--images">
                                <li>
                                    <img src="https://secureservercdn.net/160.153.138.53/9x4.fc4.myftpupload.com/wp-content/uploads/2022/02/WhatsApp-Image-2022-02-18-at-5.54.56-PM.jpeg" alt="" />
                                    <span>
                                            <svg class="icon icon--small">
                                                <use href="/assets/images/sprite.svg#xmark"></use>
                                            </svg>
                                        </span>
                                </li>
                                <li>
                                    <img src="https://secureservercdn.net/160.153.138.53/9x4.fc4.myftpupload.com/wp-content/uploads/2022/02/WhatsApp-Image-2022-02-18-at-5.54.56-PM.jpeg" alt="" />
                                    <span>
                                            <svg class="icon icon--small">
                                                <use href="/assets/images/sprite.svg#xmark"></use>
                                            </svg>
                                        </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div>
                        <h2>Vaši podaci</h2>
                        <i>Ove informacije neće biti vidljive na ePozivnici</i>
                        <input name="userName" class="full-width" type="text" placeholder="Ime i prezime" required
                        <?php if (isset($data['formData']['userName'])) { echo 'value="'.$data['formData']['userName'].'"'; } ?> />
                        <input name="email" class="full-width" type="text" placeholder="Email" required
                        <?php if (isset($data['formData']['email'])) { echo 'value="'.$data['formData']['email'].'"'; } ?> />
                        <input name="phone" class="full-width" type="number" placeholder="Telefon" required
                        <?php if (isset($data['formData']['phone'])) { echo 'value="'.$data['formData']['phone'].'"'; } ?> />
                    </div>
                    <div class="button">
                        <input type="submit" value="Pošalji" onClick="highlightRequired()" />
                    </div>
                </form>
            </div>
            <div class="order--preview" style="height:500px">
                <!--<img src="<?=$data['template']->getImage()?>" alt="<?=$data['template']->getLabel()?>" /> -->
                <iframe id="preview" width="100%" height="100%" src="/pozivnica/preview/<?=$data['template']->getId()?>/"></iframe>
            </div>
        </section>
    </div>
    <div class="to-top">
        <span>Top</span>
        <svg class="icon">
            <use href="/assets/images/sprite.svg#angle-up" />
        </svg>
    </div>
</main>