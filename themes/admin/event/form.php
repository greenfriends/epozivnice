<?php
/* @var array $data */
/* @var int $loggedInRole */
/* @var string $protectedPath */

$this->layout('layout::standard') ?>
<div class="row">
    <div class="col-lg-12">
    <?php if ($data['model']):?>
        <h1 class="page-header"><?=$this->t('Izmeni pozivnicu:')?> <?=$data['model']->getSlug()?></h1>
    <?php else: ?>
        <h1 class="page-header"><?=$this->t('Kreiraj pozivnicu:')?></h1>
    <?php endif; ?>
    </div>
</div>
<form action="/event/<?= ($data['model']) ? 'update/' . $data['model']?->getId() : 'create' ?>/"
      class="inlineForm" method="POST" enctype="multipart/form-data" id="userForm" autocomplete="off">
    <?=$this->formToken(); ?>
    <div class="row">
    <div class="col-lg-12">
    <div class="panel panel-primary">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-4">
                <h4>Tip proslave</h4>
                <div class="form-group">
                    <?php if (!$data['model']):?>
                    <select class="form-control" name="type">
                        <option value="1">Rođendan</option>
                        <option value="2">Venčanje</option>
                    </select>
                    <?php else: ?>
                    <p class="inputDesign form-control"><?=\Epozivnice\Event\Model\Event::getHrType($data['model']->getType())?></p>
                    <input type="hidden" name="type" value="<?=$data['model']->getType()?>" />
                    <?php endif; ?>
                </div>

                <div class="row form-group">
                    <div class="col-lg-4">
                        Boja
                        <select name="color">
                        <?php foreach(\Epozivnice\Template\Model\Color::$colors as $class => $label): ?>
                            <option value="<?=$class?>" <?php if ($class === $data['model']->getTemplate()->getColor()) {
                                echo 'selected'; } ?>><?=$label?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-8">
                        Slug
                        <input name="slug" value="<?=$data['model']?->getSlug() ?>" class="" />
                    </div>
                </div>

                <h4>Vaši podaci</h4>
                <p><em>Ove informacije nece biti vidljive na ePozivnici</em></p>
                <div class="form-group">
                    <input placeholder="Ime i prezime" name="userName" value="<?=$data['model']?->getUserName()?>" class="form-control inputDesign" />
                    <input placeholder="Email" name="email" value="<?=$data['model']?->getEmail()?>" class="form-control inputDesign" />
                    <input placeholder="Telefon" name="phone" value="<?=$data['model']?->getPhone()?>" class="form-control inputDesign" />
                </div>

                <h4>Ime i prezime mladenaca</h4>
                <div class="form-group row">
                    <div class="col-lg-6">
                        <input placeholder="Ime mlade" name="host1FirstName" value="<?=$data['model']?->getHost1FirstName() ?>" class="form-control inputDesign" />
                        <input placeholder="Ime mladozenje" name="host2FirstName" value="<?=$data['model']?->getHost2FirstName() ?>" class="form-control inputDesign" />
                    </div>
                    <div class="col-lg-6">
                        <input placeholder="Prezime mlade" name="host1LastName" value="<?=$data['model']?->getHost1LastName() ?>" class="form-control inputDesign" />
                        <input placeholder="Prezime mladozenje" name="host2LastName" value="<?=$data['model']?->getHost2LastName() ?>" class="form-control inputDesign" />
                    </div>
                </div>

            </div>
            <div class="col-lg-4">
                <h4>Datum svadbe</h4>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <?php
                        $day = '';
                        $month = '';
                        $year = '';
                        if ($data['model']) {
                            $year = $data['model']->getEventDate()->format('Y');
                            $month = $data['model']->getEventDate()->format('m');
                            $day = $data['model']->getEventDate()->format('d');
                            $hour = $data['model']->getEventDate()->format('H');
                            $minute = $data['model']->getEventDate()->format('i');
                        }
                        ?>
                        <input value="<?=$day?>" name="eventDate[day]" placeholder="Dan" type="number" min="1" step="1" max="31" class="form-control inputDesign">
                    </div>
                    <div class="col-lg-4">
                        <input value="<?=$month?>" name="eventDate[month]" placeholder="Mesec" type="number" min="1" step="1" max="12" class="form-control inputDesign">
                    </div>
                    <div class="col-lg-4">
                        <input value="<?=$year?>" name="eventDate[year]" placeholder="Godina" type="number" min="<?=(int)date('Y')?>" step="1" max="<?=(int)date('Y')+3?>" class="form-control inputDesign">
                    </div>
                </div>
                <h4>Početak</h4>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <input value="<?=$hour?>" name="eventDate[hour]" placeholder="Sati" type="number" min="1" step="1" max="23" class="form-control inputDesign">
                    </div>
                    <div class="col-lg-4">
                        <input value="<?=$minute?>" name="eventDate[minute]" placeholder="Minuta" type="number" min="0" step="15" max="59" class="form-control inputDesign">
                    </div>
                </div>

                <h4>Lokacija proslave</h4>
                <div class="form-group">
                    <input placeholder="Naziv objekta" name="locationName" value="<?=$data['model']?->getLocationName()?>" class="form-control inputDesign" />
                    <input placeholder="Grad" name="locationCity" value="<?=$data['model']?->getLocationCity()?>" class="form-control inputDesign" />
                    <input placeholder="Adresa" name="locationAddress" value="<?=$data['model']?->getLocationAddress()?>" class="form-control inputDesign" />
                    <textarea placeholder="Opis (opciono)" name="description" class="form-control inputDesign"><?=$data['model']?->getDescription()?></textarea>
                </div>

                <h4>Slika na vrhu</h4>
                <div class="form-group">
                    <?php if ($data['model']): ?>
                        <img width="50%" src="<?=$data['model']->getTemplate()->getImage1('deskSingle', false)?>" />
                    <?php endif; ?>
                    <input type="file" name="image1" value="" class="form-control inputDesign" />
                </div>

                <h4>Slika na dnu</h4>
                <div class="form-group">
                    <?php if ($data['model']): ?>
                        <img width="50%" src="<?=$data['model']->getTemplate()->getImage2('deskSingle', false)?>" />
                    <?php endif; ?>
                    <input type="file" name="image2" value="" class="form-control inputDesign" />
                </div>
            </div>
            <div class="col-lg-4">
                <h4>O nama (opciono)</h4>
                <div class="form-group row">
                    <textarea placeholder="Kako smo se upoznali" name="aboutHosts[description][]" class="form-control inputDesign"><?=$data['model']?->getAboutHosts('description', 0)?></textarea>
                    <div class="col-lg-4">
                        <input name="aboutHosts[day][]" placeholder="Dan" type="number" min="1" step="1" max="31" value="<?=$data['model']?->getAboutHosts('day', 0)?>">
                    </div>
                    <div class="col-lg-4">
                        <input name="aboutHosts[month][]" placeholder="Mesec" type="number" min="1" step="1" max="12" value="<?=$data['model']?->getAboutHosts('month', 0)?>">
                    </div>
                    <div class="col-lg-4">
                        <input name="aboutHosts[year][]" placeholder="Godina" type="number" min="2000" step="1" max="<?=date('Y')?>" value="<?=$data['model']?->getAboutHosts('year', 0)?>">
                    </div>
                </div>

                <h4>O nama (opciono)</h4>
                <div class="form-group row">
                    <textarea placeholder="Prosidba" name="aboutHosts[description][]" class="form-control inputDesign"><?=$data['model']?->getAboutHosts('description', 1)?></textarea>
                    <div class="col-lg-4">
                        <input name="aboutHosts[day][]" placeholder="Dan" type="number" min="1" step="1" max="31" value="<?=$data['model']?->getAboutHosts('day', 1)?>">
                    </div>
                    <div class="col-lg-4">
                        <input name="aboutHosts[month][]" placeholder="Mesec" type="number" min="1" step="1" max="12" value="<?=$data['model']?->getAboutHosts('month', 1)?>">
                    </div>
                    <div class="col-lg-4">
                        <input name="aboutHosts[year][]" placeholder="Godina" type="number" min="2000" step="1" max="<?=date('Y')?>" value="<?=$data['model']?->getAboutHosts('year', 1)?>">
                    </div>
                </div>

                <h4>Galerija (opciono)</h4>
                <div class="form-group">

                </div>
            </div>
            <!-- /.col-lg-4 (nested) -->
            <input type="hidden" name="eventId" value="<?=$data['model']?->getId()?>" />
            <input type="hidden" name="templateId" value="<?=$data['model']?->getTemplate()->getTemplateId()?>" />
        </div>
        <div class="row">
            <div class="form-group">
                <input type="submit" value="<?=$this->t('Submit')?>" class="btn btn-success submitButtonSize" />
            </div>
        </div>
    </div>
        <!-- /.panel-body -->
    </div>
        <!-- /.panel -->
    </div>
        <!-- /.row -->
    </div>
</form>