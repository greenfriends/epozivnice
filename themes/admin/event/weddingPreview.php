<?php
/* @var array $data */
/* @var int $loggedInRole */
/* @var string $protectedPath */

$this->layout('layout::event') ?>
<link href="https://fonts.googleapis.com/css2?family=Parisienne&family=Pinyon+Script&family=Roboto+Serif:wght@100;300&family=Roboto:wght@400&display=swap" rel="stylesheet">
<header class="header">
    <div class="container">
        <h1>Kate & Charlie</h1>
    </div>
</header>
<main style="overflow: hidden">
    <section class="image image--top"
        style="background-image: url(

        )">
        <h2>Venčaćemo se!</h2>
        <div class="image--arrows">
            <span>
                <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.1.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M224 416c-8.188 0-16.38-3.125-22.62-9.375l-192-192c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0L224 338.8l169.4-169.4c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25l-192 192C240.4 412.9 232.2 416 224 416z"/></svg>
            </span>
            <span>
                <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.1.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M224 416c-8.188 0-16.38-3.125-22.62-9.375l-192-192c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0L224 338.8l169.4-169.4c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25l-192 192C240.4 412.9 232.2 416 224 416z"/></svg>
            </span>
            <span>
                <svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.1.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M224 416c-8.188 0-16.38-3.125-22.62-9.375l-192-192c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0L224 338.8l169.4-169.4c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25l-192 192C240.4 412.9 232.2 416 224 416z"/></svg>
            </span>
        </div>
    </section>
    <div class="container">
        <section class="names">
            <div class="names--content">
                <h3>Kate Miller</h3>
                <span>&</span>
                <h3>Charlie Luwis</h3>
            </div>
        </section>
        <section class="block">
            <h4>#KateandCharlie</h4>
            <div class="block--date">
                <div>8</div>
                <div>Jun</div>
                <div>2019</div>
            </div>
            <h4>19:30 časova - NekoMesto na adresi u Beogradu</h4>
            <p>This floral wedding website template gets its name from a Zola couple who met in grade school. But it
                wasn’t until they rekindled in Napa in their 20s that they realized they had found their forever. A
                beautiful design for an elegant outdoor affair.</p>
            <div class="block--attend">
                <span>Molimo Vas potvrdite dolazak</span>
                <div class="success">Uspešno ste potvrdili dolazak. <strong>Hvala!</strong></div>
                <form action="">
                    <input type="text" placeholder="Ime i prezime" tabindex="1" />
                    <select tabindex="2">
                        <option>+0</option>
                        <option>+1</option>
                    </select>
                    <select tabindex="3">
                        <option>Deca</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                    <div>
                        <input type="submit" value="Pošalji" tabindex="4" />
                    </div>
                </form>
            </div>
        </section>
        <section class="about about--first">
            <div class="about--date">
                <h2>Kako smo se upoznali</h2>
                <span>8.Jun.2019</span>
            </div>
            <div class="about--text">
                <p>This floral wedding website template gets its name from a Zola couple who met in grade school. But it
                    wasn’t until they rekindled in Napa in their 20s that they realized they had found their forever. A
                    beautiful design for an elegant outdoor affair.</p>
            </div>
        </section>
        <section class="about">
            <div class="about--date">
                <h2>Prosidba</h2>
                <span>8.Jun.2019</span>
            </div>
            <div class="about--text">
                <p>This floral wedding website template gets its name from a Zola couple who met in grade school. But it
                    wasn’t until they rekindled in Napa in their 20s that they realized they had found their forever. A
                    beautiful design for an elegant outdoor affair.</p>
            </div>
        </section>
        <section class="block">
            <h4>Naši momenti</h4>
            <div class="block--date">
                <div class="clear">Galerija</div>
            </div>
        </section>
        <div class="gallery">
            <a href="https://assets.codepen.io/12005/windmill.jpg" data-tab="2">
                <img src="https://assets.codepen.io/12005/windmill.jpg" alt="A windmill" />
            </a>
            <a href="https://assets.codepen.io/12005/suspension-bridge.jpg" data-tab="2">
                <img src="https://assets.codepen.io/12005/suspension-bridge.jpg" alt="The Clifton Suspension Bridge" />
            </a>
            <a href="https://assets.codepen.io/12005/sunset.jpg" data-tab="3">
                <img src="https://assets.codepen.io/12005/sunset.jpg" alt="Sunset and boats" />
            </a>
            <a href="https://assets.codepen.io/12005/snowy.jpg" data-tab="4">
                <img src="https://assets.codepen.io/12005/snowy.jpg" alt="a river in the snow" />
            </a>
            <a href="https://assets.codepen.io/12005/bristol-balloons1.jpg" data-tab="3">
                <img src="https://assets.codepen.io/12005/bristol-balloons1.jpg" alt="a single checked balloon" />
            </a>
            <a href="https://assets.codepen.io/12005/dog-balloon.jpg" data-tab="4">
                <img src="https://assets.codepen.io/12005/dog-balloon.jpg" alt="a hot air balloon shaped like a dog" />
            </a>
            <a href="https://assets.codepen.io/12005/abq-balloons.jpg" data-tab="2">
                <img src="https://assets.codepen.io/12005/abq-balloons.jpg" alt="View from a hot air balloon of other balloons" />
            </a>
            <a href="https://assets.codepen.io/12005/disney-balloon.jpg" data-tab="3">
                <img src="https://assets.codepen.io/12005/disney-balloon.jpg" alt="a balloon fairground ride" />
            </a>
            <a href="https://assets.codepen.io/12005/bristol-harbor.jpg" data-tab="2">
                <img src="https://assets.codepen.io/12005/bristol-harbor.jpg" alt="sunrise over a harbor" />
            </a>
            <a href="https://assets.codepen.io/12005/bristol-balloons2.jpg"data-tab="4">
                <img src="https://assets.codepen.io/12005/bristol-balloons2.jpg" alt="three hot air balloons in a blue sky" />
            </a>
            <a href="https://assets.codepen.io/12005/toronto.jpg" data-tab="3">
                <img src="https://assets.codepen.io/12005/toronto.jpg" alt="the Toronto light up sign at night" />
            </a>
        </div>
        <div class="initials">
            <span>K&C</span>
        </div>
    </div>
</main>