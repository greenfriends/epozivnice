<?php
/* @var array $data */
/* @var int $loggedInRole */
/* @var string $protectedPath */

$this->layout('layout::event') ?>
<link href="https://fonts.googleapis.com/css2?family=Cormorant+Garamond:wght@300;400&family=Imperial+Script&family=Roboto+Serif:wght@100;300&family=Roboto:wght@400&display=swap" rel="stylesheet">
<main>
    <section class="image" style="background-image: url('/assets/css/images/wedding1/test.jpeg')">
        <div class="image--content">
            <h1>Venčaćemo se!</h1>
            <h2 id="date">14. Avgust 2019. godine</h2>
            <div class="clock"></div>
        </div>
        <div class="image--opacity"></div>
        <div class="image--bg image--top-left"></div>
        <div class="image--bg image--top-right"></div>
        <div class="image--bg image--bottom-left"></div>
        <div class="image--bg image--bottom-right"></div>
        <a class="image--scroll" href="#sadrzaj"><b></b>Scroll</a>
    </section>
    <div class="content">
        <div class="container">
            <section class="names">
                <div class="names--content">
                    <h3><span id="host1FirstName">Kate</span> <span id="host1LastName">Miller</span></h3>
                    <span>&</span>
                    <h3><span id="host2FirstName">Charlie</span> <span id="host2LastName">Lewis</span></h3>
                </div>
            </section>
            <section class="block">
                <div class="block--info">
                    <div>
                        <span>gde</span>
                        <h4><i id="locationName">NekoMesto</i>, <i id="locationAddress">negde</i>
                            u <i id="locationCity">Beograd</i></h4>
                    </div>
                    <div>
                        <span>kad</span>
                        <h3><i id="eventDate[hour]">19</i>:<i id="eventDate[minute]">30</i> h <br />
                            <i id="eventDate[day]">18</i>. <i id="eventDate[month]">Jul</i> <i id="eventDate[year]">2018</i>.</h3>
                    </div>
                </div>
                <p id="description">This floral wedding website template gets its name from a Zola couple who met in grade school. But it
                    wasn’t until they rekindled in Napa in their 20s that they realized they had found their forever. A
                    beautiful design for an elegant outdoor affair.</p>
            </section>
<!--            <section class="block">-->
<!--                <div class="attend--outer">-->
<!--                    <div class="attend">-->
<!--                        <div class="attend--title">-->
<!--                            <span>Vidimo se!</span>-->
<!--                            <h4>Potvrdite dolazak</h4>-->
<!--                            <div class="attend--title-img">-->
<!--                                <img src="/assets/css/images/wedding1/flower.png" alt="Flower" />-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <form action="">-->
<!--                            <input type="text" placeholder="Ime i prezime" tabindex="1" />-->
<!--                            <select tabindex="2">-->
<!--                                <option selected disabled>Broj gositiju</option>-->
<!--                                <option>0</option>-->
<!--                                <option>+1</option>-->
<!--                            </select>-->
<!--                            <select tabindex="3">-->
<!--                                <option selected disabled>Deca</option>-->
<!--                                <option>1</option>-->
<!--                                <option>2</option>-->
<!--                                <option>3</option>-->
<!--                                <option>4</option>-->
<!--                                <option>5</option>-->
<!--                            </select>-->
<!--                            <button type="submit" tabindex="4">Potvrdite dolazak</button>-->
<!--                        </form>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </section>-->
        </div>
        <div class="image--container">
            <div class="container">
                <section class="about about--first">
                    <div class="about--date">
                        <h2>Kako smo se upoznali</h2>
                        <span>19.Maj.2019.</span>
                    </div>
                    <div class="about--text">
                        <p>This floral wedding website template gets its name from a Zola couple who met in grade school. But it
                            wasn’t until they rekindled in Napa in their 20s that they realized they had found their forever. A
                            beautiful design for an elegant outdoor affair.</p>
                    </div>
                </section>
                <section class="about">
                    <div class="about--date">
                        <h2>Prosidba</h2>
                        <span>19.Maj.2019.</span>
                    </div>
                    <div class="about--text">
                        <p>This floral wedding website template gets its name from a Zola couple who met in grade school. But it
                            wasn’t until they rekindled in Napa in their 20s that they realized they had found their forever. A
                            beautiful design for an elegant outdoor affair.</p>
                    </div>
                </section>
            </div>
        </div>
        <div class="container">
            <section class="block">
                <h3>Galerija</h3>
                <h4>Naši momenti</h4>
            </section>
            <div class="gallery">
                <a href="https://assets.codepen.io/12005/windmill.jpg">
                    <img src="https://assets.codepen.io/12005/windmill.jpg" alt="A windmill" />
                    <span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.0.0-beta3 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2021 Fonticons, Inc. --><path d="M432 256C432 260.4 428.4 264 424 264h-192v192c0 4.422-3.578 8.006-8 8.006S216 460.4 216 456v-192h-192c-4.422 0-8-3.572-8-7.994C16 251.6 19.58 248 24 248h192v-192c0-4.422 3.578-7.994 8-7.994S232 51.58 232 56v192h192C428.4 248 432 251.6 432 256z"/></svg>
                        </span>
                </a>
                <a href="https://assets.codepen.io/12005/suspension-bridge.jpg">
                    <img src="https://assets.codepen.io/12005/suspension-bridge.jpg" alt="The Clifton Suspension Bridge" />
                    <span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.0.0-beta3 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2021 Fonticons, Inc. --><path d="M432 256C432 260.4 428.4 264 424 264h-192v192c0 4.422-3.578 8.006-8 8.006S216 460.4 216 456v-192h-192c-4.422 0-8-3.572-8-7.994C16 251.6 19.58 248 24 248h192v-192c0-4.422 3.578-7.994 8-7.994S232 51.58 232 56v192h192C428.4 248 432 251.6 432 256z"/></svg>
                        </span>
                </a>
                <a href="https://assets.codepen.io/12005/sunset.jpg">
                    <img src="https://assets.codepen.io/12005/sunset.jpg" alt="Sunset and boats" />
                    <span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.0.0-beta3 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2021 Fonticons, Inc. --><path d="M432 256C432 260.4 428.4 264 424 264h-192v192c0 4.422-3.578 8.006-8 8.006S216 460.4 216 456v-192h-192c-4.422 0-8-3.572-8-7.994C16 251.6 19.58 248 24 248h192v-192c0-4.422 3.578-7.994 8-7.994S232 51.58 232 56v192h192C428.4 248 432 251.6 432 256z"/></svg>
                        </span>
                </a>

            </div>
        </div>
        <section class="names">
            <div class="names--content">
                <h3>Charlie & Kate</h3>
            </div>
        </section>
    </div>
</main>