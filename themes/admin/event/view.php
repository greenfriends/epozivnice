<?php $this->layout('layout::standard') ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">ePozivnice:</h1>
    </div>
</div>

<div class="row biggerBottonMargin">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <!-- /.panel-heading -->
            <div class="panel-body panel-bodyWhite">
                <div class="row">
                    <div class="col-lg-2">
                        <a href="/event/form/"><?=$this->t('Create new')?></a>
                    </div>
                    <div class="col-lg-2">
                        <select class="form-control mandatorySelect typeFilter">
                            <option><?=$this->t('--- Izaberi tip ---')?></option>
                            <option value="1"><?=$this->t('Rodjendan')?></option>
                            <option value="2"><?=$this->t('Vencanje')?></option>
                        </select>
                    </div>
                    <button class="btn btn-primary filter"><?=$this->t('Filtriraj')?></button>
                </div>
                <table class="table table-striped table-bordered table-hover" id="activityTable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Tip</th>
                        <th>Status</th>
                        <th>Korisnik</th>
                        <th>Kreirano</th>
                        <th>Izmenjeno</th>
                        <th>Akcija</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    /* @var \Epozivnice\Event\Model\Event $event */
                    foreach ($data['models'] as $event): ?>
                    <tr>
                        <td><a href="/event/form/<?=$event->getId()?>/"><?=$event->getSlug()?></a></td>
                        <td><?=\Epozivnice\Event\Model\Event::getHrType($event->getType())?> <?=$event->getType()?></td>
                        <td><?=\Epozivnice\Event\Model\Event::getHrStatus($event->getStatus())?></td>
                        <td><?=$event->getUserName()?></td>
                        <td><?=$event->getCreatedAt()->format('d/m/Y H:i')?></td>
                        <td><?=$event->getUpdatedAt()->format('d/m/Y H:i')?></td>
                        <td>
                            <a href="/event/delete/<?=$event->getId()?>/">Obriši</a>
                            <a href="/<?=$event->getUri()?>/" target="_blank">Pregledaj</a>
                        <?php switch ($event->getStatus()) {
                            case \Epozivnice\Event\Model\Event::STATUS_PAID: ?>
                            <a href="/event/status/<?=$event->getId()?>/?status=<?=\Epozivnice\Event\Model\Event::STATUS_DISABLED?>">Deaktiviraj</a>
                                <?php break;
                            case \Epozivnice\Event\Model\Event::STATUS_PENDING_PAYMENT: ?>
                            <a href="/event/status/<?=$event->getId()?>/?status=<?=\Epozivnice\Event\Model\Event::STATUS_PAID?>">Aktiviraj</a>
                                <?php break;
                            case \Epozivnice\Event\Model\Event::STATUS_DISABLED: ?>
                            <a href="/event/status/<?=$event->getId()?>/?status=<?=\Epozivnice\Event\Model\Event::STATUS_PAID?>">Re-Aktiviraj</a>
                                <?php break;
                        } ?>
                        </td>
                    </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>