<?php
declare(strict_types=1);
namespace Epozivnice\Event\Repository;

use Epozivnice\Event\Mapper\Event as Mapper;
use Epozivnice\Event\Mapper\Template;
use Epozivnice\Event\Model\Event as Model;
use Epozivnice\Event\Service\Image;
use Skeletor\Mapper\NotFoundException;
use Laminas\Config\Config;
use Skeletor\Repository\RepositoryInterface;

class EventRepository implements RepositoryInterface
{
    /**
     * @var Mapper
     */
    private $mapper;

    /**
     * @var \DateTime
     */
    private $dt;

    /**
     * @var Config
     */
    private $config;

    private $template;

    /**
     * userRepository constructor.
     *
     * @param Mapper $mapper
     * @param \DateTime $dt
     * @param Config $config
     */
    public function __construct(Mapper $mapper, Template $template, \DateTime $dt, Config $config)
    {
        $this->mapper =$mapper;
        $this->dt = $dt;
        $this->config = $config;
        $this->template = $template;
    }

    /**
     * Fetches a list of UserEntity models.
     *
     * @param array $params
     *
     * @return array
     */
    public function fetchAll($params = array(), $limit = null, $order = null): array
    {
        $items = [];
        foreach ($this->mapper->fetchAll($params, $limit, $order) as $data) {
            $items[] = $this->make($data);
        }

        return $items;
    }

    /**
     * @param $id
     * @return Model
     * @throws NotFoundException
     */
    public function getById($id): Model
    {
        return $this->make($this->mapper->fetchById((int) $id));
    }

    /**
     * @param $data
     * @return Model
     * @throws NotFoundException
     */
    public function update($data, $addressData = null): Model
    {
        $about = $data['aboutHosts'];
        $date = $data['eventDate'];
        $templateData = [
            'templateId' => $data['templateId'],
            'image1' => $data['image1'],
            'image2' => $data['image2'],
            'color' => $data['color'],
        ];
        unset($data['templateId']);
        unset($data['color']);
        unset($data['image1']);
        unset($data['image2']);
        unset($data['aboutHosts']);
        unset($data['eventDate']);
        $this->dt->setDate((int) $date['year'], (int) $date['month'], (int) $date['day']);
        $this->dt->setTime((int) $date['hour'], (int) $date['minute']);
        $data['eventDate'] = $this->dt->format('Y-m-d H:i:s');
        $data['aboutHosts'] = serialize($about);
        if (strlen($templateData['image1']) === 0) {
            unset($templateData['image1']);
        }
        if (strlen($templateData['image2']) === 0) {
            unset($templateData['image2']);
        }
        $tplData = $this->template->fetchAll(['eventId' => $data['eventId']])[0];
        $this->mapper->update($data);
        $templateData['eventTemplateId'] = $tplData['eventTemplateId'];
        $this->template->update($templateData);

        return $this->getById($data['eventId']);
    }

    public function getByEvent($eventId)
    {
        return $this->template->fetchAll(['eventId' => $eventId])[0];
    }

    public function create($data, $addressData = null): Model
    {
        $about = $data['aboutHosts'];
        $date = $data['eventDate'];
        $templateData = [
            'templateId' => $data['templateId'],
            'image1' => $data['image1'],
            'image2' => $data['image2'],
            'color' => $data['color'],
        ];
        unset($data['color']);
        unset($data['templateId']);
        unset($data['image1']);
        unset($data['image2']);
        unset($data['aboutHosts']);
        unset($data['eventDate']);

        $data['eventDate'] = $date['year'].'-'.$date['month'].'-'.$date['day'] .' '. $date['hour'] .':'. $date['minute'] .':00';
        if ($data['type'] === Model::TYPE_WEDDING) {
            $data['slug'] = $this->slugify($data['host1FirstName'] .'-'. $data['host2FirstName']);
        } elseif ($data['type'] === Model::TYPE_BIRTHDAY) {
            $data['slug'] = $this->slugify($data['host1FirstName']);
        }
        $data['hash'] = $this->generateHash($data['slug'] . $data['userName'] . $data['email']);
        $data['aboutHosts'] = serialize($about);
        $eventId = $this->mapper->insert($data);
        $templateData['eventId'] = $eventId;
        $this->template->insert($templateData);

        return $this->getById($eventId);
    }

    private function slugify($text, $divider = '-')
    {
        $text = preg_replace('~[^\pL\d]+~u', $divider, $text);
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        $text = trim($text, $divider);
        // remove duplicate divider
        $text = preg_replace('~-+~', $divider, $text);
        $text = strtolower($text);
        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    public function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt', 'eventDate'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }

        $data['aboutHosts'] = unserialize($data['aboutHosts']);
        $tplData = $this->template->fetchAll(['eventId' => $data['eventId']])[0];
        $template = new \Epozivnice\Event\Model\Template(
            $tplData['eventTemplateId'], $tplData['eventId'], $tplData['templateId'], $tplData['image1'], $tplData['image2'],
            $tplData['color'], null, null
        );

        return new Model(
            $data['eventId'],
            $data['host1FirstName'],
            $data['host1LastName'],
            $data['host2FirstName'],
            $data['host2LastName'],
            $data['description'],
            $data['aboutHosts'],
            $data['eventDate'],
            $data['locationName'],
            $data['locationAddress'],
            $data['locationCity'],
            $data['gallery'],
            $data['userName'],
            $data['email'],
            $data['phone'],
            $data['slug'],
            $data['type'],
            $data['status'],
            $data['title'],
            $template,
            $data['hash'],
            $data['createdAt'],
            $data['updatedAt']
        );
    }

    public function getBySlug($slug, $key = null)
    {
        $params['slug'] = $slug;
        if ($key) {
            $params['hash'] = $key;
        } else {
            $params['status'] = Model::STATUS_PAID;
        }
        $data = $this->mapper->fetchAll($params);
        if (!count($data)) {
            throw new NotFoundException('Pozivnica nije pronadjena.');
        }

        return $this->make($data[0]);
    }

    /**
     * @param $tenantId
     * @return bool
     */
    public function delete($tenantId): bool
    {
        return $this->mapper->delete($tenantId);
    }

    public function setStatus($status, $entityId)
    {
        return $this->mapper->updateField('status', $status, $entityId);
    }

    private function generateHash($input, $length = 8)
    {
        // Create a raw binary sha256 hash and base64 encode it.
        $hash_base64 = base64_encode(hash('sha256', $input, true));
        // Replace non-urlsafe chars to make the string urlsafe.
        $hash_urlsafe = strtr( $hash_base64, '+/', '-_' );
        $hash_urlsafe = rtrim( $hash_urlsafe, '=' );

        return substr($hash_urlsafe, 0, $length);
    }
}
