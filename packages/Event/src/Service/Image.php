<?php
declare(strict_types=1);
namespace Epozivnice\Event\Service;

use \GuzzleHttp\Psr7\UploadedFile;

class Image
{
    public static $cropSizes = [
        'deskSingle' => 800,
        'mobileSingle' => 400,
        'deskList' => 250,
    ];

    public function __construct()
    {

    }

    /**
     * @param UploadedFile $uploadedFile
     * @param $itemId
     *
     * @return string saved file name
     * @throws \Exception
     */
    public function processUploadedFile(UploadedFile $uploadedFile, $itemId)
    {
        return $this->processImage($this->createTmpImageFromUploadedFile($uploadedFile), $itemId);
    }

    private function createTmpImageFromUploadedFile(UploadedFile $uploadedFile)
    {
        $msg = false;
        switch ($uploadedFile->getError()) {
            case UPLOAD_ERR_INI_SIZE:
                $msg = 'Image is bigger than allowed size.';
                break;
            case UPLOAD_ERR_FORM_SIZE:
                $msg = 'Image is bigger than allowed size.';
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                $msg = 'Unable to find tmp directory.';
                break;
            case UPLOAD_ERR_CANT_WRITE:
                $msg = 'Could not write file, check permissions.';
                break;
        }
        if ($msg) {
            throw new \Exception($msg);
        }
        $targetPath = IMAGES_PATH . '/' . md5($uploadedFile->getClientFilename());
        $uploadedFile->moveTo($targetPath);

        return $targetPath;
    }

    private function processImage($tmpFileName, $itemId)
    {
        $tmpImage = new \Imagick($tmpFileName);

        $jpg = new \Imagick();
        $jpg->newImage($tmpImage->getImageWidth(), $tmpImage->getImageHeight(), "white");
        $jpg->compositeimage($tmpImage, \Imagick::COMPOSITE_OVER, 0, 0);
        $jpg->setImageFormat('jpg');
        $targetPath = IMAGES_PATH . '/' . $itemId . '/';
        if (!is_dir($targetPath)) {
            mkdir($targetPath);
        }
        $fileName = basename(explode('.', $tmpFileName)[0]);
        $targetPath .= $fileName;
        $jpg->writeImage($targetPath . '-org.jpg');
        \imagewebp(\imagecreatefromjpeg($targetPath . '-org.jpg'), $targetPath . '-org.webp');
        if ($jpg->getImageWidth() > 2000) {
            $scaled = clone $jpg;
            $scaled->scaleImage((int) ceil($jpg->getImageWidth() / 2), (int) ceil($jpg->getImageHeight() / 2));
            $scaled->writeImage($targetPath . '-org-scaled.jpg');
            \imagewebp(\imagecreatefromjpeg($targetPath . '-org-scaled.jpg'), $targetPath . '-org-scaled.webp');
        }
//        $jpg = $this->getBestFitCrop($jpg);
        $jpg->writeImage($targetPath . '.jpg');
        $this->createThumbnails($jpg, $targetPath);
        \imagewebp(\imagecreatefromjpeg($targetPath . '.jpg'), $targetPath . '.webp');

        unlink($tmpFileName);

        return $fileName . '.jpg';
    }

    private function getBestFitCrop(\Imagick $image)
    {
        $width = $image->getImageWidth();
        $height = $image->getImageHeight();
        $maxSize = min($width, $height); // we need cube, so this is crop width and height
        if ($width >= $height) {
            $wOffset = (int) ceil(($width - $height) / 2);
            $hOffset = 0;
        } else {
            $hOffset = (int) ceil(($height - $width) / 2);
            $wOffset = 0;
        }

        if (!$image->cropImage($maxSize, $maxSize, $wOffset, $hOffset)) {
            var_dump('could not save image');
            die();
        }

        return $image;
    }

    private function createThumbnails(\Imagick $image, $targetPath)
    {
        foreach (static::$cropSizes as $cropSize) {
            $crop = clone $image;
            $newHeight = (int) ($cropSize / ($image->getImageWidth() / $image->getImageHeight()));
            $crop->resizeImage($cropSize, $newHeight, \Imagick::FILTER_CATROM, 1);
            $cropPath = sprintf('%s_%s.jpg', $targetPath, $cropSize);
            $crop->writeImage($cropPath);
            $crop->destroy();
            $cropWebpPath = sprintf('%s_%s.webp', $targetPath, $cropSize);
            \imagewebp(\imagecreatefromjpeg($cropPath), $cropWebpPath);
        }
    }
}