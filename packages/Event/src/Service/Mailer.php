<?php

namespace Epozivnice\Event\Service;

use Laminas\Mail\Message;
use Laminas\Mime\Message as MimeMessage;
use Laminas\Mime\Mime;
use Laminas\Mime\Part;

class Mailer extends \Skeletor\Mailer\Service\Mailer
{

    public function sendEventPrivateLink($username, $url, $email, $hash)
    {
        $body = sprintf('<p>Postovani %s,</p>', $username);
        $body .= '<p>Ispod mozete pronaci link do stranice za upravljanje i pregled vase pozivnice:</p>';
        $url = $url . '/' . $hash . '/';
        $body .= '<a href="' . $url . '">' . $url . '</a>';
        $mimeMessage = new MimeMessage();
        $text = new Part();
        $text->type = Mime::TYPE_HTML;
        $text->charset = 'utf-8';
        $text->setContent($body);
        $mimeMessage->addPart($text);

        $message = new Message();
        $message
            ->addTo($email)
            ->setFrom($this->config->offsetGet('mailer')->from)
            ->setSubject(sprintf('Link za upravljanje pozivnicom - Epozivnice.rs'))
            ->setBody($mimeMessage);
        $this->send($message);
    }
}