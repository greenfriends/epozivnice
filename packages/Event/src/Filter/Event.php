<?php
declare(strict_types=1);
namespace Epozivnice\Event\Filter;

use Laminas\Filter\ToInt;
use \Psr\Http\Message\ServerRequestInterface as Request;
use Skeletor\Filter\FilterInterface;
use Volnix\CSRF\CSRF;
use Epozivnice\Event\Validator\Event as EventValidator;
use Laminas\I18n\Filter\Alnum;
use Skeletor\Validator\ValidatorException;

class Event implements FilterInterface
{
    /**
     * @var EventValidator
     */
    private $validator;

    public function __construct(EventValidator $validator)
    {
        $this->validator = $validator;
    }

    public function getErrors()
    {
        return $this->validator->getMessages();
    }

    public function filter(Request $request): array
    {
        $alnum = new Alnum(true);
        $int = new ToInt();
        $postData = $request->getParsedBody();
        $data = [
            'eventId' => (isset($postData['eventId'])) ? $int->filter($postData['eventId']) : null,
            'host1FirstName' => $alnum->filter($postData['host1FirstName']),
            'host1LastName' => $alnum->filter($postData['host1LastName']),
            'host2FirstName' => (isset($postData['host2FirstName'])) ? $alnum->filter($postData['host2FirstName']) : 'n/a',
            'host2LastName' => (isset($postData['host2LastName'])) ? $alnum->filter($postData['host2LastName']) : 'n/a',
            'description' => $postData['description'],
            'aboutHosts' => (isset($postData['aboutHosts'])) ? $postData['aboutHosts'] : '',
            'eventDate' => $postData['eventDate'],
            'locationName' => $postData['locationName'],
            'locationAddress' => $postData['locationAddress'],
            'locationCity' => $postData['locationCity'],
            'userName' => $postData['userName'],
            'email' => $postData['email'],
            'phone' => $postData['phone'],
            'slug' => (isset($postData['slug'])) ? $postData['slug'] : null,
            'type' => $int->filter($postData['type']),
            'color' => $postData['color'],
            'templateId' => $postData['templateId'],

//            'topImage' => $postData['topImage'],
//            'bottomImage' => $postData['bottomImage'],
            'status' => \Epozivnice\Event\Model\Event::STATUS_PENDING_PAYMENT,

            'title' => '',
            'gallery' => '',
            'image1' => '',
            'image2' => '',
            CSRF::TOKEN_NAME => $postData[CSRF::TOKEN_NAME],
        ];
        if (!$this->validator->isValid($data)) {
            throw new ValidatorException(serialize($this->validator->getValidatedData()));
        }
        unset($data[CSRF::TOKEN_NAME]);

        return $data;
    }

}