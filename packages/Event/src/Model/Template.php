<?php
declare(strict_types=1);
namespace Epozivnice\Event\Model;

use Epozivnice\Event\Service\Image;
use Skeletor\Model\Model;

class Template extends Model
{

    private $eventTemplateId;

    private $eventId;

    private $templateId;

    private $image1;

    private $image2;

    private $color;

    /**
     * @param $eventTemplateId
     * @param $eventId
     * @param $templateId
     * @param $image1
     * @param $image2
     * @param $color
     */
    public function __construct($eventTemplateId, $eventId, $templateId, $image1, $image2, $color, $createdAt, $updatedAt)
    {
        parent::__construct($createdAt, $updatedAt);
        $this->eventTemplateId = $eventTemplateId;
        $this->eventId = $eventId;
        $this->templateId = $templateId;
        $this->image1 = $image1;
        $this->image2 = $image2;
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->eventTemplateId;
    }

    /**
     * @return mixed
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @return mixed
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    public function getImage1($name = 'deskSingle', $webp = false, $index = 1)
    {
        if (strlen($this->image1) === 0) {
            return '';
        }
        $ext = 'jpg';
        if ($webp) {
            $ext = 'webp';
        }
        return sprintf('/images/event/%s/%s_%s.%s', $this->eventId, pathinfo($this->image1)['filename'], Image::$cropSizes[$name], $ext);
    }

    public function getImage2($name = 'deskSingle', $webp = false, $index = 1)
    {
        if (strlen($this->image2) === 0) {
            return '';
        }
        $ext = 'jpg';
        if ($webp) {
            $ext = 'webp';
        }
        return sprintf('/images/event/%s/%s_%s.%s', $this->eventId, pathinfo($this->image2)['filename'], Image::$cropSizes[$name], $ext);
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

}