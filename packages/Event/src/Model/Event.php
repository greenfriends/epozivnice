<?php
declare(strict_types=1);
namespace Epozivnice\Event\Model;

use Skeletor\Model\Model;

class Event extends Model
{
    const TYPE_WEDDING = 1;
    const TYPE_BIRTHDAY = 2;

    const STATUS_PENDING_PAYMENT = 1;
    const STATUS_PAID = 2;
    const STATUS_EXPIRED = 3;
    const STATUS_DELETED = 4;
    const STATUS_DISABLED = 5;

    private $eventId;

    private $host1FirstName;

    private $host1LastName;

    private $host2FirstName;

    private $host2LastName;

    private $description;

    // date / textarea; date / textarea
    private $aboutHosts;

    private $eventDate;

    private $locationName;
    private $locationAddress;
    private $locationCity;

    private $hash;

    private $gallery;

    private $userName;
    private $email;
    private $phone;

    private $slug;
    private $type;
    private $status;

    /**
     * @var Template
     */
    private $template;

    /**
     * @param $eventId
     * @param $host1FirstName
     * @param $host1LastName
     * @param $host2FirstName
     * @param $host2LastName
     * @param $description
     * @param $aboutHosts
     * @param $eventDate
     * @param $locationName
     * @param $locationAddress
     * @param $locationCity
     * @param $gallery
     * @param $userName
     * @param $email
     * @param $phone
     * @param $slug
     * @param $type
     * @param $status
     */
    public function __construct(
        $eventId, $host1FirstName, $host1LastName, $host2FirstName, $host2LastName, $description, $aboutHosts, $eventDate,
        $locationName, $locationAddress, $locationCity, $gallery, $userName, $email, $phone, $slug, $type, $status,
        $title, $template, $hash, $createdAt, $updatedAt
    ) {
        parent::__construct($createdAt, $updatedAt);
        $this->eventId = $eventId;
        $this->host1FirstName = $host1FirstName;
        $this->host1LastName = $host1LastName;
        $this->host2FirstName = $host2FirstName;
        $this->host2LastName = $host2LastName;
        $this->description = $description;
        $this->title = $title;
        $this->aboutHosts = $aboutHosts;
        $this->eventDate = $eventDate;
        $this->locationName = $locationName;
        $this->locationAddress = $locationAddress;
        $this->locationCity = $locationCity;
        $this->gallery = $gallery;
        $this->userName = $userName;
        $this->email = $email;
        $this->phone = $phone;
        $this->slug = $slug;
        $this->type = $type;
        $this->status = $status;
        $this->template = $template;
        $this->hash = $hash;
    }

    public static function getHrType($type)
    {
        return static::getHrTypes()[$type];
    }

    public static function getHrTypes()
    {
        return [
            static::TYPE_BIRTHDAY => 'Rodjendan',
            static::TYPE_WEDDING => 'Venčanje',
        ];
    }

    public static function getHrStatus($status)
    {
        return static::getHrStatuses()[$status];
    }

    public static function getHrStatuses()
    {
        return [
            static::STATUS_PENDING_PAYMENT => 'Čeka se uplata',
            static::STATUS_PAID => 'Plaćeno / Aktivno',
            static::STATUS_EXPIRED => 'Isteklo',
            static::STATUS_DELETED => 'Obrisano',
            static::STATUS_DISABLED => 'Ugašeno',
        ];
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    public function getTemplate(): Template
    {
        return $this->template;
    }

    public function getId()
    {
        return (int) $this->eventId;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getHost1FirstName()
    {
        return $this->host1FirstName;
    }

    /**
     * @return mixed
     */
    public function getHost1LastName()
    {
        return $this->host1LastName;
    }

    /**
     * @return mixed
     */
    public function getHost2FirstName()
    {
        return $this->host2FirstName;
    }

    /**
     * @return mixed
     */
    public function getHost2LastName()
    {
        return $this->host2LastName;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getAboutHosts($part, $index)
    {
        if (isset($this->aboutHosts[$part][$index])) {
            return $this->aboutHosts[$part][$index];
        }
        return '';
    }

    /**
     * @return mixed
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }

    /**
     * @return mixed
     */
    public function getLocationName()
    {
        return $this->locationName;
    }

    /**
     * @return mixed
     */
    public function getLocationAddress()
    {
        return $this->locationAddress;
    }

    /**
     * @return mixed
     */
    public function getLocationCity()
    {
        return $this->locationCity;
    }

    /**
     * @return mixed
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    public function getUri()
    {
//        $type = iconv('utf-8', 'us-ascii//TRANSLIT', $this->getHrType($this->type));
        $t = \Transliterator::create('Any-Latin; Latin-ASCII');
        $type = $t->transliterate($this->getHrType($this->type));
        return strtolower($type . '/' . $this->slug);
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return (int) $this->type;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return (int) $this->status;
    }
}