<?php

namespace Epozivnice\Event\Validator;

use Skeletor\Validator\ValidatorInterface;
use Epozivnice\Event\Repository\EventRepository;
use Volnix\CSRF\CSRF;

/**
 * Class Event.
 * User validator.
 *
 * @package Epozivnice\Event\Validator
 */
class Event implements ValidatorInterface
{
    /**
     * @var EventRepository
     */
    private $repo;

    private $data;

    /**
     * @var CSRF
     */
    private $csrf;

    private $messages = [];

    /**
     * User constructor.
     *
     * @param EventRepository $repo
     * @param CSRF $csrf
     */
    public function __construct(EventRepository $repo, CSRF $csrf)
    {
        $this->repo = $repo;
        $this->csrf = $csrf;
    }

    /**
     * Validates provided data, and sets errors with Flash in session.
     *
     * @param $data
     *
     * @return bool
     */
    public function isValid(array $data): bool
    {
        $valid = true;
        if (strlen($data['host1FirstName']) < 3 || strlen($data['host1FirstName']) > 60) {
            $this->messages['host1FirstName'][] = 'Ime mora imati između 3 i 60 karaktera.';
            $valid = false;
            unset($data['host1FirstName']);
        }
        if (strlen($data['host2FirstName']) < 3 || strlen($data['host2FirstName']) > 60) {
            $this->messages['host2FirstName'][] = 'Ime mora imati između 3 i 60 karaktera.';
            $valid = false;
            unset($data['host2FirstName']);
        }
        if (strlen($data['host1LastName']) < 3 || strlen($data['host1LastName']) > 60) {
            $this->messages['host1LastName'][] = 'Prezime mora imati između 3 i 60 karaktera.';
            $valid = false;
            unset($data['host1LastName']);
        }
        if (strlen($data['host2LastName']) < 3 || strlen($data['host2LastName']) > 60) {
            $this->messages['host1LastName'][] = 'Prezime mora imati između 3 i 60 karaktera.';
            $valid = false;
            unset($data['host2LastName']);
        }
        if (strlen($data['locationName']) < 3 || strlen($data['locationName']) > 60) {
            $this->messages['locationName'][] = 'Naziv objekta mora imati između 3 i 60 karaktera.';
            $valid = false;
            unset($data['locationName']);
        }
        if (strlen($data['locationCity']) < 3 || strlen($data['locationCity']) > 60) {
            $this->messages['locationCity'][] = 'Grad mora imati između 3 i 60 karaktera.';
            $valid = false;
            unset($data['locationCity']);
        }
        if (strlen($data['locationAddress']) < 3 || strlen($data['locationAddress']) > 60) {
            $this->messages['locationAddress'][] = 'Adresa mora imati između 3 i 60 karaktera.';
            $valid = false;
            unset($data['locationAddress']);
        }
        if (strlen($data['userName']) < 3 || strlen($data['userName']) > 60) {
            $this->messages['userName'][] = 'Ime i prezime moraju imati između 3 i 60 karaktera.';
            $valid = false;
            unset($data['userName']);
        }
        if (strlen($data['email']) < 3 || strlen($data['email']) > 60) {
            $this->messages['email'][] = 'Email mora imati između 3 i 60 karaktera.';
            $valid = false;
            unset($data['email']);
        }
        if (strlen($data['phone']) < 3 || strlen($data['phone']) > 20) {
            $this->messages['phone'][] = 'Telefon mora imati između 3 i 20 karaktera.';
            $valid = false;
            unset($data['phone']);
        }

        if (!$this->csrf->validate($data)) {
            $this->messages['general'][] = 'Vaša sesija je istekla, probajte ponovo.';
            $valid = false;
        }
        $this->data = $data;

        return $valid;
    }

    public function getValidatedData()
    {
        return $this->data;
    }

    /**
     * Hack used for testing
     *
     * @return string
     */
    public function getMessages(): array
    {
        return $this->messages;
    }
}
