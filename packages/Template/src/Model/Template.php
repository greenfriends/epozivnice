<?php
declare(strict_types=1);
namespace Epozivnice\Template\Model;

use Epozivnice\Event\Service\Image;
use Skeletor\Model\Model;

class Template extends Model
{
    const TYPE_WEDDING = 1;
    const TYPE_BIRTHDAY = 2;

    private $templateId;

    private $label;

    private $templateFile;

    private $image;

    private $slug;

    private $filters;

    private $cssFile;

    private $jsFile;

    private $type;

    private $price;

    private $description;

    /**
     * @param $templateId
     * @param $label
     * @param $templateFile
     * @param $template
     */
    public function __construct(
        $templateId, $label, $templateFile, $cssFile, $jsFile, $filters, $image, $type, $slug, $price, $description,
        $createdAt, $updatedAt
    ) {
        parent::__construct($createdAt, $updatedAt);
        $this->templateId = $templateId;
        $this->label = $label;
        $this->templateFile = $templateFile;
        $this->cssFile = $cssFile;
        $this->jsFile = $jsFile;
        $this->image = $image;
        $this->filters = $filters;
        $this->type = $type;
        $this->slug = $slug;
        $this->price = $price;
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return mixed
     */
    public function getImage($name = 'deskSingle', $webp = false, $index = 1)
    {
        if (!$this->image || strlen($this->image) === 0) {
            return '';
        }
        $ext = 'jpg';
        if ($webp) {
            $ext = 'webp';
        }
        return sprintf(
            '/images/template/%s/%s_%s.%s',
            $this->templateId,
            pathinfo($this->image)['filename'],
            Image::$cropSizes[$name], $ext
        );
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return (int) $this->type;
    }

    public static function getHrType($type)
    {
        return static::getHrTypes()[$type];
    }

    public static function getHrTypes()
    {
        return [
            static::TYPE_BIRTHDAY => 'Rođendan',
            static::TYPE_WEDDING => 'Venčanje',
        ];
    }

    /**
     * @return mixed
     */
    public function getFilters($type)
    {
        if (!$this->filters) {
            return [];
        }
        return unserialize($this->filters)[$type];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return (int) $this->templateId;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return mixed
     */
    public function getTemplateFile()
    {
        return $this->templateFile;
    }

    /**
     * @return mixed
     */
    public function getCssFile()
    {
        return $this->cssFile;
    }

    /**
     * @return mixed
     */
    public function getJsFile()
    {
        return $this->jsFile;
    }

}