<?php

namespace Epozivnice\Template\Model;

class Color
{
    /**
     * 'css class' => 'label'
     *
     * @var string[]
     */
    public static $colors = [
        'red' => 'Crvena',
        'orange' => 'Orange',
        'yellow' => 'Yellow',
        'green' => 'Green',
        'blue' => 'Plava',
        'purple' => 'Purple',
        'pink' => 'Pink',
        'brown' => 'Brown',
        'beige' => 'Beige',
        'multi' => 'Multi',
        'gray' => 'Gray',
        'black' => 'Black',
        'white' => 'White',
        'rose-gold' => 'Rose gold',
        'silver' => 'Silver',
    ];
}