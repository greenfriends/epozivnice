<?php
declare(strict_types=1);
namespace Epozivnice\Template\Mapper;

use Skeletor\Mapper\MysqlCrudMapper;
use Skeletor\Mapper\PDOWrite;

class Template extends MysqlCrudMapper
{
    public function __construct(PDOWrite $pdo)
    {
        parent::__construct($pdo, 'template');
    }

    public function getStats()
    {
        $sql = "SELECT sum( 
case
when type = 1 then 1
end) as weddingCount,
       sum( 
case
when type = 2 then 1
end) as birthdayCount
FROM `{$this->tableName}` ";
        $stmt = $this->driver->prepare($sql);
        $stmt->execute();

        return  $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}