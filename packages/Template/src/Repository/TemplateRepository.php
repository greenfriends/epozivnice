<?php
declare(strict_types=1);
namespace Epozivnice\Template\Repository;

use Epozivnice\Template\Mapper\Template as Mapper;
use Epozivnice\Template\Model\Template as Model;
use Skeletor\Mapper\NotFoundException;
use Laminas\Config\Config;
use Skeletor\Repository\RepositoryInterface;

class TemplateRepository implements RepositoryInterface
{
    /**
     * @var Mapper
     */
    private $mapper;

    /**
     * @var \DateTime
     */
    private $dt;

    /**
     * @var Config
     */
    private $config;

    /**
     * userRepository constructor.
     *
     * @param Mapper $mapper
     * @param \DateTime $dt
     * @param Config $config
     */
    public function __construct(Mapper $mapper, \DateTime $dt, Config $config)
    {
        $this->mapper =$mapper;
        $this->dt = $dt;
        $this->config = $config;

    }

    public function getStats(): array
    {
        return $this->mapper->getStats()[0];
    }

    /**
     * Fetches a list of UserEntity models.
     *
     * @param array $params
     *
     * @return array
     */
    public function fetchAll($params = array(), $limit = null, $order = null): array
    {
        $items = [];
        foreach ($this->mapper->fetchAll($params, $limit, $order) as $data) {
            $items[] = $this->make($data);
        }

        return $items;
    }

    public function getBySlug($slug)
    {
        $data = $this->mapper->fetchAll(['slug' => $slug])[0];

        return $this->make($data);
    }

    /**
     * @param $id
     * @return Model
     * @throws NotFoundException
     */
    public function getById($id): Model
    {
        return $this->make($this->mapper->fetchById((int) $id));
    }

    /**
     * @param $data
     * @return Model
     * @throws NotFoundException
     */
    public function update($data, $addressData = null): Model
    {
        return $this->getById($this->mapper->update($data));
    }

    public function create($data, $addressData = null): Model
    {
        return $this->getById($this->mapper->insert($data));
    }

    /**
     * Factory method
     *
     * @param $itemData
     * @return Model
     */
    public function make($itemData): Model
    {
        $data = [];
        foreach ($itemData as $name => $value) {
            if (in_array($name, ['createdAt', 'updatedAt'])) {
                $data[$name] = null;
                if ($value) {
                    if (strtotime($value)) {
                        $dt = clone $this->dt;
                        $dt->setTimestamp(strtotime($value));
                        $data[$name] = $dt;
                    } else {
                        $data[$name] = null;
                    }
                }
            } else {
                $data[$name] = $value;
            }
        }

        if (!isset($data['createdAt'])) {
            $data['createdAt'] = null;
        }
        if (!isset($data['updatedAt'])) {
            $data['updatedAt'] = null;
        }

        return new Model(
            $data['templateId'],
            $data['label'],
            $data['templateFile'],
            $data['cssFile'],
            $data['jsFile'],
            $data['filters'],
            $data['image'],
            $data['type'],
            $data['slug'],
            $data['price'],
            $data['description'],
            $data['createdAt'],
            $data['updatedAt']
        );
    }

    /**
     * @param $templateId
     * @return bool
     */
    public function delete($templateId): bool
    {
        return $this->mapper->delete($templateId);
    }
}
