<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateEventTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `event` (
  `eventId` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `host1FirstName` varchar(64) NOT NULL,
  `host1LastName` varchar(64) NOT NULL,
  `host2FirstName` varchar(64) NOT NULL,
  `host2LastName` varchar(64) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `aboutHosts` text NOT NULL,
  `eventDate` datetime NOT NULL,
  `locationName` varchar(64) NOT NULL,
  `locationAddress` varchar(64) NOT NULL,
  `locationCity` varchar(64) NOT NULL,
  `gallery` text NULL,
  `userName` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `slug` varchar(64) NOT NULL,
  `type` int(1) NOT NULL,
  `hash` varchar(8) NOT NULL,
  `status` int(1) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `event`");
    }
}

// alter table event add column `hash` varchar(8) not null;