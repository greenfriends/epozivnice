<?php


use Phinx\Migration\AbstractMigration;

class CreateUserTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `user` (
  `userId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `firstName` varchar(128) NULL,
  `lastName` varchar(128) NULL,
  `lastLogin` datetime DEFAULT NULL,
  `ipv4` int UNSIGNED DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `role` int(1) unsigned DEFAULT '1',
  `displayName` varchar(128) DEFAULT NULL,
  `isActive` int(1) DEFAULT '0',
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
        $this->query("ALTER TABLE `user` 
          ADD INDEX `role_INDEX` (`role` ASC),
          ADD INDEX `isActive_INDEX` (`isActive` ASC)
          ;");
        // insert default admin
        $pass = '$2y$10$GGArVO/7.xPDg6D5Kl6GHeELUg2Dnod68ynkFaZ7R2Vfx/K1oZ96O'; // testtest
        $sql = "INSERT INTO `user` (
`email`, `password`, `isActive`, `displayName`
) VALUES (
'test@example.com', '{$pass}', '1', 'admin');";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `user`");
    }
}
