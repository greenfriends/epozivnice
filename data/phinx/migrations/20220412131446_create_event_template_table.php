<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateEventTemplateTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `eventTemplate` (
  `eventTemplateId` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `eventId` int(16) NOT NULL,
  `templateId` int(16) NOT NULL,
  `image1` varchar(128) NULL,
  `image2` varchar(128) NULL,
  `color` varchar(128) NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`eventTemplateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `eventTemplate`");
    }
}
