<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateTemplateTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `template` (
  `templateId` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(32) NOT NULL,
  `templateFile` varchar(32) NOT NULL,
  `cssFile` varchar(32) NULL,
  `jsFile` varchar(32) NULL,
  `image` varchar(64) NULL,
  `slug` varchar(32) NULL,
  `filters` text NULL,
  `description` text NOT NULL,
  `price` int(9) NOT NULL,
  `type` int(1) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`templateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `template`");
    }
}
