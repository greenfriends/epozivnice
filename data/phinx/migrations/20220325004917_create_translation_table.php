<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateTranslationTable extends AbstractMigration
{
    public function up()
    {
        $sql = "CREATE TABLE `translation` (
  `translationId` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `language` varchar(8) NOT NULL,
  `languageId` int(1) NOT NULL,
  `group` varchar(16) NULL,
  `originalString` varchar(256) NOT NULL,
  `translatedString` varchar(256) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`translationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";
        $this->query($sql);
    }

    public function down()
    {
        $this->query("DROP TABLE `translation`");
    }
}
